<?php
include_once("init.php");

?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Add Purchase</title>

    <!-- Stylesheets -->

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="js/date_pic/date_input.css">
    <link rel="stylesheet" href="lib/auto/css/jquery.autocomplete.css">

    <!-- Optimize for mobile devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- jQuery & JS files -->
    <?php include_once("tpl/common_js.php"); ?>
    <script src="js/script.js"></script>
    <script src="js/date_pic/jquery.date_input.js"></script>
    <script src="lib/auto/js/jquery.autocomplete.js "></script>
<script language="javascript" type="text/javascript">
function getXMLHTTP() { //fuction to return the xml http object
		var xmlhttp=false;	
		try{
			xmlhttp=new XMLHttpRequest();
		}
		catch(e)	{		
			try{			
				xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e){
				try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch(e1){
					xmlhttp=false;
				}
			}
		}
		 	
		return xmlhttp;
    }
	
	function getState(countryId) {		
		
		var strURL="findState.php?country="+countryId;
		var req = getXMLHTTP();
		
		if (req) {
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('statediv').innerHTML=req.responseText;
						document.getElementById('citydiv').innerHTML='<input type="text" name="" id="stock">';
						} else {
						alert("Problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			req.open("GET", strURL, true);
			req.send(null);
		}		
	}
	function getCits(stateId) {		
		var strURL="findCity.php?&state="+stateId;
		var req = getXMLHTTP();
		
		if (req) {
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('citydiv').innerHTML=req.responseText;						
					} else {
						alert("Problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			req.open("GET", strURL, true);
			req.send(null);
		}
				
	}
</script>

    <script type="text/javascript">
        $(function () {
            document.getElementById('bill_no').focus();
            $("#supplier").autocomplete("supplier1.php", {
                width: 160,
                autoFill: true,
                selectFirst: true
            });
            
            $("#quty").blur(function () {
                if (document.getElementById('item').value == "") {
                    document.getElementById('item').focus();
                }
            });
            $("#supplier").blur(function () {


                $.post('check_supplier_details.php', {stock_name1: $(this).val()},
                    function (data) {

                        $("#address").val(data.address);
                        $("#contact1").val(data.contact1);

                        if (data.address != undefined)
                            $("#0").focus();

                    }, 'json');


            });
            $('#test1').jdPicker({
				 date_format:"dd/mm/YYYY"
			});



            var hauteur = 0;
            $('.code').each(function () {
                if ($(this).height() > hauteur) hauteur = $(this).height();
            });

            $('.code').each(function () {
                $(this).height(hauteur);
            });
        });

    </script>
    <script>
        /*$.validator.setDefaults({
         submitHandler: function() { alert("submitted!"); }
         });*/
        $(document).ready(function () {
            if (document.getElementById('item') === "") {
                document.getElementById('item').focus();
            }
            // validate signup form on keyup and submit
            $("#form1").validate({
                rules: {
                    bill_no: {
                        required: false,
                        minlength: 3,
                        maxlength: 200
                    },
                    stockid: {
                        required: true
                    },
                    
                    supplier: {
                        required: true,
                    }
                },
                messages: {
                    supplier: {
                        required: "Please Enter Supplier"
                    },
                    stockid: {
                        required: "Please Enter Stock ID"
                    },
                    
                    bill_no: {
                        required: "Please Enter Bill Number",
                        minlength: "Bill Number must consist of at least 3 characters"
                    }
                }
            });

        });
        function numbersonly(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
            if (unicode != 8 && unicode != 46 && unicode != 37 && unicode != 27 && unicode != 38 && unicode != 39 && unicode != 40 && unicode != 9) { //if the key isn't the backspace key (which we should allow)
                if (unicode < 48 || unicode > 57)
                    return false
            }
        }


    </script>
    <script type="text/javascript">
        
        function total_amount() {
            
            document.getElementById('total').value = parseFloat(document.getElementById('stock').value) + parseFloat(document.getElementById('quty').value)
           
            if (document.getElementById('item').value === "") {
                document.getElementById('item').focus();
            }
        }
        
        function formatCurrency(fieldObj) {
            if (isNaN(fieldObj.value)) {
                return false;
            }
            fieldObj.value = '$ ' + parseFloat(fieldObj.value).toFixed(2);
            return true;
        }
        
    </script>

</head>
<body>

<!-- TOP BAR -->
<?php include_once("tpl/top_bar.php"); ?>
<!-- end top-bar -->


<!-- HEADER -->
<div id="header-with-tabs">

    <div class="page-full-width cf">

        <ul id="tabs" class="fl">
            <li><a href="dashboard.php" class="dashboard-tab">Dashboard</a></li>
            <li><a href="view_sales.php" class="sales-tab">Sales</a></li>
            <li><a href="view_customers.php" class=" customers-tab">Customers</a></li>
            <li><a href="view_purchase.php" class="purchase-tab">Purchase</a></li>
            <li><a href="view_supplier.php" class=" supplier-tab">Supplier</a></li>
            <li><a href="view_stock_availability.php" class="active-tab stock-tab">Stocks / Products</a></li>
            <li><a href="view_payments.php" class="payment-tab">Payments / Outstandings</a></li>
            <li><a href="" class="report-tab">Reports</a></li>
        </ul>
        <!-- end tabs -->

        <!-- Change this image to your own company's logo -->
        <!-- The logo will automatically be resized to 30px height. -->
        <a href="#" id="company-branding-small" class="fr"><img src="<?php if (isset($_SESSION['logo'])) {
                echo "upload/" . $_SESSION['logo'];
            } else {
                echo "upload/posnic.png";
            } ?>" alt="Point of Sale"/></a>

    </div>
    <!-- end full-width -->

</div>
<!-- end header -->


<!-- MAIN CONTENT -->
<div id="content">

    <div class="page-full-width cf">

        <div class="side-menu fl">

            <h3>Purchase Management</h3>
            <ul>
            	<li><a href="increase_stock.php">Add/Increase Stock</a></li>
                <li><a href="add_stock.php">Add Stock Detail</a></li>
                <li><a href="add_category.php">Add Stock Category</a></li>
                <li><a href="view_category.php">view Stock Category</a></li>
                <li><a href="view_stock_availability.php">view Stock Available</a></li>
                <li><a href="view_stock.php">Add View Purchase Detail</a></li>
                
            </ul>
        </div>
        <!-- end side-menu -->

        <div class="side-content fr">

            <div class="content-module">

                <div class="content-module-heading cf">

                    <h3 class="fl">Add Purchase</h3>
                    <span class="fr expand-collapse-text">Click to collapse</span>
                    <span class="fr expand-collapse-text initial-expand">Click to expand</span>

                </div>
                <!-- end content-module-heading -->

                <div class="content-module-main cf">


                    <?php
                    //Gump is libarary for Validatoin
                    if (isset($_GET['msg'])) {
                        echo $_GET['msg'];
                    }
                    if (isset($_POST['supplier']) and isset($_POST['item'])) {
                        $_POST = $gump->sanitize($_POST);
                        $gump->validation_rules(array(
                            'supplier' => 'required|max_len,100|min_len,3'


                        ));

                        $gump->filter_rules(array(
                            'supplier' => 'trim|sanitize_string|mysqli_escape'


                        ));

                        $validated_data = $gump->run($_POST);
                        $supplier = "";
                        $stockid = "";
                        $stock_name = "";
                        
                        $bill_no = "";


                        if ($validated_data === false) {
                            echo $gump->get_readable_errors(true);
                        } else {
                            $username = $_SESSION['username'];

                            $stockid = mysqli_real_escape_string($db->connection, $_POST['stockid']);

                            $bill_no = mysqli_real_escape_string($db->connection, $_POST['bill_no']);
                            $supplier = mysqli_real_escape_string($db->connection, $_POST['supplier']);
                            $address = mysqli_real_escape_string($db->connection, $_POST['address']);
                            $contact = mysqli_real_escape_string($db->connection, $_POST['contact']);
                            $stock_name = $_POST['item'];

                            $count = $db->countOf("supplier_details", "supplier_name='$supplier'");
                            if ($count == 0) {
                                $db->query("insert into supplier_details(supplier_name,supplier_address,supplier_contact1) values('$supplier','$address','$contact')");
                            }
                            $quty = $_POST['quty'];
                            $total = $_POST['total'];
                            $chid = $_POST['stock1id'];
                            $autoid = $_POST['stockid'];
                            $autoid1 = $autoid;
                            $selected_date = $_POST['date'];
                            $selected_date = strtotime($selected_date);
                            $date = date('Y-m-d H:i:s', $selected_date);
                            
							 $stname = $db->queryUniqueValue("SELECT name FROM stock_avail WHERE id='$chid'");
							 $cat = $db->queryUniqueValue("SELECT category FROM stock_avail WHERE id='$chid'");
							 $typ = $db->queryUniqueValue("SELECT type FROM stock_avail WHERE id='$chid'");
                                    
                                    
                                    $db->execute("UPDATE stock_avail SET quantity='$total' WHERE id='$chid'");
                                    $db->query("INSERT INTO stock_entries(material,category,stock_id,stock_name,stock_supplier_name,quantity,date,username,type,billnumber) VALUES ('$typ','$cat','$autoid1','$stname','$supplier','$quty','$date','$username','entry','$bill_no')");
                                    //INSERT INTO `stock`.`stock_entries` (`id`, `stock_id`, `stock_name`, `stock_supplier_name`, `category`, `quantity`, `company_price`, `selling_price`, `opening_stock`, `closing_stock`, `date`, `username`, `type`, `salesid`, `total`, `payment`, `balance`, `mode`, `description`, `due`, `subtotal`, `count1`)
                                    //VALUES (NULL, '$autoid1', '$stock_name[$i]', '$supplier', '', '$quantity', '$brate', '$srate', '$amount', '$amount1', '$mysqldate', 'sdd', 'entry', 'Sa45', '432.90', '2342.90', '24.34', 'cash', 'sdflj', '2010-03-25 12:32:02', '45645', '1');


                            $msg = "<br><font color=green size=6px >Stock Increased successfully Ref: [" . $_POST['stockid'] . "] !</font>";
                            echo "<script>window.location = 'add_purchase.php?msg=$msg';</script>";
                        }

                    }

                    ?>

                    <form name="form1" method="post" id="form1" action="">
                        <input type="hidden" id="posnic_total">

                        <p><strong>Add Stock/Product </strong> - Add New ( Control +2)</p>
                        <table class="form" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <?php
                                $max = $db->maxOfAll("id", "stock_entries");
                                $max = $max + 3;
                                $autoid = "PR" . $max . "";
                                ?>
                                <td>Stock ID:</td>
                                <td><input name="stockid" type="text" id="stockid" readonly maxlength="200"
                                           class="round default-width-input" style="width:130px "
                                           value="<?php echo $autoid ?>"/></td>

                                <td>Date:</td>
                                <td><input name="date" id="test1" placeholder="" value="<?php echo date('d-m-Y'); ?>"
                                           type="text" id="name" maxlength="200" class="round default-width-input"/>
                                </td>
                                <td><span class="man">*</span>Bill No:</td>
                                <td><input name="bill_no" placeholder="ENTER BILL NO" type="text" id="bill_no"
                                           maxlength="200" class="round default-width-input" style="width:120px "/></td>

                            </tr>
                            <tr>
                                <td><span class="man">*</span>Supplier:</td>
                                <td><input name="supplier" placeholder="ENTER SUPPLIER" type="text" id="supplier"
                                           maxlength="200" class="round default-width-input" style="width:130px "/></td>

                                <td>Address:</td>
                                <td><input name="address" placeholder="ENTER ADDRESS" type="text" id="address"
                                           maxlength="200" class="round default-width-input"/></td>

                                <td>contact:</td>
                                <td><input name="contact" placeholder="ENTER CONTACT" type="text" id="contact1"
                                           maxlength="200" class="round default-width-input"
                                           onkeypress="return numbersonly(event)" style="width:120px "/></td>

                                    <input type="hidden" id="guid">
                       				<input type="hidden" id="edit_guid">
                            </tr>
                        </table>
                        
                        <table class="form">
                            <tr>
                                <td>Item:</td>
                                <td>Type:</td>
                                <td>Add Quantity:</td>
                                <td>Available Stock:</td>
                                <td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total</td>
                                <td>&nbsp; </td>
                            </tr>
                            <tr><?php 
							$sql = "select * from stock_details";
							$result = mysqli_query($db->connection, $sql);
							
							?>

                                <td  width="150"><select name="item" onChange="getState(this.value)" id="item">
                                    <option value="">Select Item</option>
                                    <?php while ($row=mysqli_fetch_array($result)) { ?>
                                    <option value="<?php echo $row['id']?>"><?php echo $row['stock_name'] ." ". $row['category']?></option>
                                    <?php } ?>
                                    </select></td>
                                <td ><div id="statediv"><select name="type" id="type">
                                    <option>Select Type</option>
                                        </select></div></td>
                                <td><input name="quty" type="text" id="quty" maxlength="200"
                                           class="round default-width-input my_with"
                                           onkeyup="total_amount();unique_check();"/></td>

                                
								<td ><div id="citydiv"><input type="text" name="stock" id="stock"></div></td>


                                 <td><input name="total" type="text" id="total" maxlength="200"
                                           class="round default-width-input " style="width:120px;  margin-left: 20px"/>
                          

                            </tr>
                        </table>
                        
                        
                        
                        <table class="form">
                            <tr>
                                <td>
                                    <input class="button round blue image-right ic-add text-upper" type="submit"
                                           name="Submit" value="Add">
                                </td>
                                <td> (Control + S)
                                    <input class="button round red   text-upper" type="reset" name="Reset"
                                           value="Reset"></td>
                                <td>&nbsp; </td>
                                <td>&nbsp; </td>
                            </tr>
                        </table>
                    </form>


                </div>
                <!-- end content-module-main -->


            </div>
            <!-- end content-module -->


        </div>
    </div>
    <!-- end full-width -->

</div>
<!-- end content -->

     
<!-- FOOTER --><div id="footer">
    <p>Any Queries email to <a href="mailto:jeetsarphare@outlook.com?subject=Print_gallery">jeetsarphare@outlook.com</a>.
    </p>

</div>
<!-- end footer -->

</body>
</html>
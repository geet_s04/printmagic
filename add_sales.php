<?php
include_once("init.php");

?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Add Sales</title>

    <!-- Stylesheets -->

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="js/date_pic/date_input.css">
    <link rel="stylesheet" href="lib/auto/css/jquery.autocomplete.css">

    <!-- Optimize for mobile devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- jQuery & JS files -->
    <?php include_once("tpl/common_js.php"); ?>
    <script src="js/script.js"></script>
    <script src="js/date_pic/jquery.date_input.js"></script>
    <script src="lib/auto/js/jquery.autocomplete.js "></script>
    <style>
        .sales_form{
            width: 95% !important;
        }
    </style>
<script>
function nw() {
    //document.getElementById("guid").innerHTML = Math.floor((Math.random() * 100) + 1);
}
</script>
    <script type="text/javascript">
        $(function () {

            $("#supplier").autocomplete("customer1.php", {
                width: 160,
                autoFill: true,
                selectFirst: true
            });
            
            $("#a").autocomplete("stock_detail.php", {
                width: 160,
                autoFill: true,
                selectFirst: true
            });

            $("#b").autocomplete("paper_detail.php", {
                width: 160,
                autoFill: true,
                selectFirst: true
            });

            $("#supplier").blur(function () {
                debugger;
				 $.post('check_customer_details.php', {stock_name1: $(this).val()},
                    function (data) {

                        $("#address").val(data.address);
                        $("#contact1").val(data.contact1);

                        if (data.address != undefined)
                            $("#0").focus();

                    }, 'json');


            });
            $('#test1').jdPicker({
			});


            var hauteur = 0;
            $('.code').each(function () {
                if ($(this).height() > hauteur) hauteur = $(this).height();
            });

            $('.code').each(function () {
                $(this).height(hauteur);
            });
        });

    </script>
    <script>
function nw() {
    document.getElementById("guid").value = Math.floor((Math.random() * 100) + 1);
}
</script>
    <script>
        /*$.validator.setDefaults({
         submitHandler: function() { alert("submitted!"); }
         });*/
        $(document).ready(function () {
            document.getElementById('bill_no').focus();
            // validate signup form on keyup and submit
            $("#form1").validate({
                rules: {
                    bill_no: {
                        required: false,
                        minlength: 3,
                        maxlength: 200
                    },
                    stockid: {
                        required: true
                    },
                    grand_total: {
                        required: true
                    },
                    supplier: {
                        required: true,
                    },
                    payment: {
                        required: true,
                    }
                },
                messages: {
                    supplier: {
                        required: "Please Enter Supplier"
                    },
                    stockid: {
                        required: "Please Enter Stock ID"
                    },
                    payment: {
                        required: "Please Enter Payment"
                    },
                    grand_total: {
                        required: "Add Stock Items"
                    },
                    bill_no: {
                        required: "Please Enter Bill Number",
                        minlength: "Bill Number must consist of at least 3 characters"
                    }
                }
            });

        });
        function numbersonly(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
            if (unicode != 8 && unicode != 46 && unicode != 37 && unicode != 27 && unicode != 38 && unicode != 39 && unicode != 40 && unicode != 9) { //if the key isn't the backspace key (which we should allow)
                if (unicode < 48 || unicode > 57)
                    return false
            }
        }


    </script>
    <script type="text/javascript">
        function remove_row(o) {
            var p = o.parentNode.parentNode;
            p.parentNode.removeChild(p);
        }
		function urlencode(str) 
        {
        var ret = str; 
        ret = ret.toString();
        ret = encodeURIComponent(ret);
        ret = ret.replace(/%20/g, '_');
        return ret;
        }
        function add_values() {
            if (unique_check()) {

                if (document.getElementById('edit_guid').value == "") {
                    if (document.getElementById('item').value != "" && document.getElementById('quty').value != "" && document.getElementById('total').value != "") {

                        if (document.getElementById('quty').value != 0) {
                            debugger;
                            particular = document.getElementById('particular').value;
                            //particular=urlencode(particular);
                            code = document.getElementById('item').value;
                            job_no = document.getElementById('job_no').value;
                            rates = document.getElementById('rates').value;
							var a = document.getElementById('a').value;
							if(a == ""){
							    a = "-";
							}
							//a=urlencode(a);
							var b = document.getElementById('b').value;
							if(b == ""){
							    b = "-";
							}
							//b=urlencode(b);
							cc = document.getElementById('c').value;
							//if(document.getElementById('c').checked){
							//    ischeck = "checked"
                            //    cc = "yes";
							//}else{
                            //    ischeck = "";
                            //    cc = "no";
                            //}
							//cc=urlencode(cc);
                            quty = document.getElementById('quty').value;
                            sell = document.getElementById('sell').value;
                            
                            total = document.getElementById('total').value;
                            item = document.getElementById('guid').value;
                            main_total = document.getElementById('posnic_total').value;

                            $('<tr id="'+item+'"><td><input type=hidden value="'+code+'" id="'+item+'id" ><input type=text name="stock_name[]" value="'+code+'" id="'+item+'st" style="width: 150px" readonly="readonly" class="round  my_with" ></td><td><input type=text name="job_no[]" readonly="readonly" value="'+job_no+'" id="'+item+'jn" class="round  my_with" ></td><td><input type=text name="a[]" readonly="readonly" value="'+a+'" id="'+item+'q1" class="round  my_with" ></td><td><input type=text name="b[]" readonly="readonly" value="'+b+'" id="'+item+'q2" class="round  my_with" style="text-align:right;" ></td><td><input type=text value="'+cc+'" name="c[]" readonly="readonly" id="'+item+'q3" class="round  my_with" style="text-align:right;" ></td><td><input type="text" name="particular_name[]" value="'+particular+'" id="'+item+'pn" style="width: 150px" readonly="readonly" class="round  my_with" ></td><td><input type="text" name="quty[]" readonly="readonly" value="'+quty+'" id="'+item+'q" class="round  my_with" style="text-align:right;" ></td><td><input type="text" name="rates[]" readonly="readonly" value="'+rates+'" id="'+item+'rt" class="round  my_with" style="text-align:right;" ></td><td><input type="text" name="sell[]" readonly="readonly" value="'+sell+'" id="'+item+'s" class="round  my_with" style="text-align:right;"  ></td><td><input type="text" name="jibi[]" readonly="readonly" value="'+total+'" id="'+item+'to" class="round  my_with" style="text-align:right;" ><input type="hidden" name="total[]" id="'+item+'my_tot" value="'+main_total+'"> </td><td><input type="button" value="" id="'+item+'" style="width:30px;border:none;height:30px;background:url(images/edit_new.png)" class="round" onclick="edit_stock_details(this.id)"  ></td><td><input type="button" value="" id="'+item+'" style="width:30px;border:none;height:30px;background:url(images/close_new.png)" class="round" onclick=reduce_balance("'+item+'");$(this).closest("tr").remove(); ></td></tr>').fadeIn("slow").appendTo('#item_copy_final');
                            document.getElementById('particular').value = "";
                            document.getElementById('job_no').value = "";
							document.getElementById('a').value = "";
                            document.getElementById('b').value = "";
                            document.getElementById('c').value = "";
							document.getElementById('quty').value = "";
							document.getElementById('rates').value = "";
                            document.getElementById('sell').value = "";
                            document.getElementById('total').value = "";
                            document.getElementById('item').value = "";
                            document.getElementById('guid').value = "";
                            if (document.getElementById('grand_total').value == "") {
                                document.getElementById('grand_total').value = main_total;
                            } else {
                                document.getElementById('grand_total').value = parseFloat(document.getElementById('grand_total').value) + parseFloat(main_total);
                            }
                            document.getElementById('main_grand_total').value = parseFloat(document.getElementById('grand_total').value);
                            document.getElementById(item + 'st').value = code;
                            document.getElementById(item + 'to').value = total;
                        } else {
                            alert('No Stock Available For This Item');
                        }
                    } else {
                        alert('Please Select An Item');
                    }
                } else {
                    id = document.getElementById('edit_guid').value;
                    document.getElementById(id + 'pn').value = document.getElementById('particular').value;
                    document.getElementById(id + 'st').value = document.getElementById('item').value;
                    document.getElementById(id + 'jn').value = document.getElementById('job_no').value;
					document.getElementById(id + 'q1').value = document.getElementById('a').value;
                    document.getElementById(id + 'q2').value = document.getElementById('b').value;
                    document.getElementById(id + 'q3').value = document.getElementById('c').value;
                    document.getElementById(id + 'rt').value = document.getElementById('rates').value;
                    //if(document.getElementById('c').checked){
                    //    $('#'+id+'q3').attr('checked',true);
                    //    document.getElementById(id + 'q3').value = "yes";
                    //    }else{
                    //        $('#'+id+'q3').removeAttr('checked');
                    //        document.getElementById(id + 'q3').value = "no";
                    //    }
                    document.getElementById(id + 'q').value = document.getElementById('quty').value;
                    document.getElementById(id + 's').value = document.getElementById('sell').value;
                    document.getElementById('grand_total').value = parseFloat(document.getElementById('grand_total').value) + parseFloat(document.getElementById('posnic_total').value) - parseFloat(document.getElementById(id + 'my_tot').value);
                    document.getElementById('main_grand_total').value = parseFloat(document.getElementById('grand_total').value);
                    document.getElementById(id + 'to').value = document.getElementById('total').value;
                    document.getElementById(id + 'id').value = id;

                    document.getElementById(id + 'my_tot').value = document.getElementById('posnic_total').value
                    document.getElementById('particular').value = "";
                    document.getElementById('a').value = "";
                    document.getElementById('job_no').value = "";
                    document.getElementById('b').value = "";
                    document.getElementById('c').value = "";

					document.getElementById('quty').value = "";
					document.getElementById('rates').value = "";
                    document.getElementById('sell').value = "";
                    document.getElementById('total').value = "";
                    document.getElementById('item').value = "";
                    document.getElementById('guid').value = "";
                    document.getElementById('edit_guid').value = "";
                }
            }
            discount_amount();
        }
        function total_amount() {
            balance_amount();

            document.getElementById('total').value = document.getElementById('sell').value 
            document.getElementById('posnic_total').value = document.getElementById('total').value;
            //  document.getElementById('total').value = '$ ' + parseFloat(document.getElementById('total').value).toFixed(2);
            if (document.getElementById('item').value === "") {
                document.getElementById('item').focus();
            }
        }
		 
        function edit_stock_details(id) {
            document.getElementById('particular').value = document.getElementById(id + 'pn').value;
            document.getElementById('item').value = document.getElementById(id + 'st').value;
            document.getElementById('job_no').value = document.getElementById(id + 'jn').value;
			document.getElementById('a').value = document.getElementById(id + 'q1').value;
            document.getElementById('b').value = document.getElementById(id + 'q2').value;
            document.getElementById('c').value = document.getElementById(id + 'q3').value;
            //if(document.getElementById(id + 'q3').checked){
            //    $('#c').attr('checked',true);
            //    document.getElementById('c').value = "yes";
            //    }else{
            //        $('#c').removeAttr('checked');
            //        document.getElementById('c').value = "no";
            //    }
            document.getElementById('rates').value = document.getElementById(id + 'rt').value;
            document.getElementById('quty').value = document.getElementById(id + 'q').value;
            document.getElementById('sell').value = document.getElementById(id + 's').value;
            document.getElementById('total').value = document.getElementById(id + 'to').value;

            document.getElementById('guid').value = id;
            document.getElementById('edit_guid').value = id;

        }
        function unique_check() {
            if (!document.getElementById(document.getElementById('guid').value) || document.getElementById('edit_guid').value == document.getElementById('guid').value) {
                return true;

            } else {

                alert("This Item is already added In This Purchase");
                document.getElementById('item').focus();
				document.getElementById('a').value = "";
                    document.getElementById('b').value = "";
                    document.getElementById('c').value = "";
                document.getElementById('quty').value = "";
                document.getElementById('sell').value = "";
                document.getElementById('total').value = "";
                document.getElementById('item').value = "";
                document.getElementById('guid').value = "";
                document.getElementById('edit_guid').value = "";
                return false;
            }
        }
        function quantity_chnage(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
            if (unicode != 13 && unicode != 9) {
            }
            else {
                add_values();

                document.getElementById("item").focus();

            }
            if (unicode != 27) {
            }
            else {

                document.getElementById("item").focus();
            }
        }
        function formatCurrency(fieldObj) {
            if (isNaN(fieldObj.value)) {
                return false;
            }
            fieldObj.value = '$ ' + parseFloat(fieldObj.value).toFixed(2);
            return true;
        }
		function discount_amount() {

            document.getElementById('payable_amount').value = parseFloat(document.getElementById('grand_total').value) ;
            if (parseFloat(document.getElementById('payment').value) > parseFloat(document.getElementById('payable_amount').value)) {
                document.getElementById('payment').value = parseFloat(document.getElementById('payable_amount').value);

            }

        }
        function balance_amount() {
            if (document.getElementById('payable_amount').value != "" && document.getElementById('payment').value != "") {
                data = parseFloat(document.getElementById('payable_amount').value);
                document.getElementById('balance').value = data - parseFloat(document.getElementById('payment').value);
                if (parseFloat(document.getElementById('payable_amount').value) >= parseFloat(document.getElementById('payment').value)) {

                } else {
                    if (document.getElementById('payable_amount').value != "") {
                        document.getElementById('balance').value = '000.00';
                        document.getElementById('payment').value = parseFloat(document.getElementById('payable_amount').value);
                    } else {
                        document.getElementById('balance').value = '000.00';
                        document.getElementById('payment').value = "";
                    }
                }
            } else {
                document.getElementById('balance').value = "";
            }


        }
        
        function reduce_balance(id) {
            var minus = parseFloat(document.getElementById(id + "my_tot").value);
            document.getElementById('grand_total').value = parseFloat(document.getElementById('grand_total').value) - minus;
            document.getElementById('main_grand_total').value = parseFloat(document.getElementById('grand_total').value);
            discount_amount();
            //console.log(id);
        }
       
    </script>

</head>
<body>

<!-- TOP BAR -->
<?php include_once("tpl/top_bar.php"); ?>
<!-- end top-bar -->


<!-- 
<!-- HEADER -->
<?php include_once("tpl/header.php"); ?>
<!-- end header -->


<!-- MAIN CONTENT -->
<div id="content">

    <div class="page-full-width cf">

        <div class="side-menu fl">

            <h3>Sales Management</h3>
            <ul>
                <li><a href="add_sales.php?page=sales">Add Sales</a></li>
                <li><a href="view_sales.php?page=sales">View Sales</a></li>
            </ul>

        </div>
        <!-- end side-menu -->

        <div class="side-content fr">

            <div class="content-module">

                <div class="content-module-heading cf">

                    <h3 class="fl">Add Sales</h3>
                    <span class="fr expand-collapse-text">Click to collapse</span>
                    <span class="fr expand-collapse-text initial-expand">Click to expand</span>

                </div>
                <!-- end content-module-heading -->

                <div class="content-module-main cf">


                   

                    <form name="form1" method="post" id="form1" action="add_sales_1.php">
                        <input type="hidden" id="posnic_total">
						<?php if (isset($_GET['msg'])) {
                        echo $_GET['msg'];
                    } ?>
                        <p><strong>Add Stock/Product </strong> - Add New ( Control +2)</p>
                        <table class="form" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <?php
                                $max = $db->maxOfAll("id", "stock_sales");
                                $max = $max + 1;
                                $autoid = "PM" . $max . "";
                                ?>
                                <input name="stockid" type="hidden" id="stockid" maxlength="200" readonly
                                           class="round default-width-input" style="width:130px "
                                           value="<?php echo $autoid; ?>"/>
									<input name="jobid" type="hidden" id="jobid" maxlength="200" class="round default-width-input" style="width:100% "
                                           value="<?php echo $autoid; ?>"/>	   
								
									
                                <td>Date:</td>
                                <td><input name="date" id="test1" placeholder="" value="<?php echo date('d-m-Y'); ?>"
                                           type="text" id="name" maxlength="200" class="round default-width-input"/>
                                </td>
                                <td><span class="man">*</span>Bill No:</td>
                                <td><input name="bill_no" placeholder="ENTER BILL NO" type="text" id="bill_no"
                                           maxlength="200" class="round default-width-input" style="width:100%;"/></td>
                                <td></td>
                                <td></td>

                            </tr>
                            <tr>
                                <td><span class="man">*</span>Customer:</td>
                                <td><input name="supplier" placeholder="ENTER CUSTOMER" type="text" id="supplier"
                                           maxlength="200" class="round default-width-input" style="width:100% "/></td>

                                <td>Address:</td>
                                <td><input name="address" placeholder="ENTER ADDRESS" type="text" id="address"
                                           maxlength="200" class="round default-width-input"/></td>

                                <td>Contact:</td>
                                <td><input name="contact" placeholder="ENTER CONTACT" type="text" id="contact1"
                                           maxlength="200" class="round default-width-input"
                                           onkeypress="return numbersonly(event)" style="width:300px "/></td>

                            </tr>
                        </table>
                        <input type="hidden" id="guid">
                        <input type="hidden" id="edit_guid">
                        <table class="form sales_form">
                            <tr>
                                <th>Product:</th>
                                <th>Job No:</th>
								<th>Size:</th>
								<th>Paper:</th>
								<th>Pulty:</th>
                                <th>Other:</th>
                                <th>Quantity:</th>
                                <th>Rates:</th>
                                <th>Price:</th>
                                <th> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total</th>
                                <th>&nbsp; </th>
                            </tr>
                            <tr>
                                <td><input name="" type="text" id="item" maxlength="200" onchange="nw();"
                                           class="round default-width-input " style="width: 150px"/></td>
                                <td><input name="" type="text" id="job_no" maxlength="200" class="round default-width-input " style="width: 80px"/></td>
								<td><input name="" type="text" id="a" maxlength="200" value=""
                                           class="round default-width-input " style="width: 80px"/></td>		   
								<td><input name="" type="text" id="b" maxlength="200"
                                           class="round default-width-input " style="width: 80px"/></td>
                                <td><input type="text" id="c" name="" value="" class="round default-width-input" style="width: 80px"></td>
                                <td><input name="" type="text" id="particular" maxlength="200" value=""
                                           class="round default-width-input " style="width: 150px"/></td>
                                <td><input name="" type="text" id="quty" maxlength="200"
                                           class="round default-width-input my_with"
                                           onkeyup="unique_check();"/></td>
                                <td><input name="" type="text" id="rates" maxlength="200"
                                            class="round default-width-input my_with"/></td>
								<td><input name="" type="text" id="sell" maxlength="200"
                                           class="round default-width-input my_with" onkeyup="total_amount()";/></td>

                                <td><input name="" type="hidden" id="total" maxlength="200"
                                           class="round default-width-input " style="width:120px;  margin-left: 20px"/>
                                </td>
                                <td><input type="button" onclick="add_values()" onkeyup=" balance_amount();"
                                           id="add_new_code"
                                           style="margin-left:30px; width:30px;height:30px;border:none;background:url(images/add_new.png)"
                                           class="round"></td>

                            </tr>
                        </table>
                        <div style="overflow:auto ;max-height:300px;  ">
                            <table class="form sales_form" id="item_copy_final">

                            </table>
                        </div>


                        <table class="form">
                            
                            <tr>
                                <td>&nbsp; </td>
                                
                                <td>Grand Total:<input type="hidden" readonly id="grand_total"
                                                       name="subtotal">
                                    <input type="text" id="main_grand_total" readonly
                                           class="round default-width-input" style="text-align:right;width: 120px">
                                </td>
                                <td>&nbsp; </td>
                                <td>Payable Amount:<input type="hidden" readonly id="grand_total">
                                    <input type="text" id="payable_amount" readonly name="payable"
                                           class="round default-width-input" style="text-align:right;width: 120px"></td>
                                
                            </tr>
                            <tr>
                                <td>&nbsp; </td>
                                <td><input type="hidden" class="round" onkeyup=" balance_amount(); "
                                                   onkeypress="return numbersonly(event);" name="payment" id="payment" value="0">
                                </td>

                                <td><input type="hidden" class="round" readonly id="balance"
                                                   name="balance">
                                </td>
                                <td>&nbsp; </td>

                                <td>&nbsp; </td>
                                <td>&nbsp; </td>
                                <td>    </td>
                                <td>&nbsp; </td>
                                <td>&nbsp; </td>
                                <td>&nbsp; </td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td>Status &nbsp;</td>
                                <td>
                                    <select name="mode">
                                        <option value="0">In Process</option>
                                        <option value="1">Completed</option>																				<option value="2">Delivered</option>
                                    </select>
                                </td>
                                <td>&nbsp; </td>
                                <td>&nbsp; </td>
                            </tr>
                        </table>
                        <table class="form">
                            <tr>
                                <td>
                                    <input class="button round blue image-right ic-add text-upper" type="submit"
                                           name="Submit" value="Add" >
                                </td>
                                <td> (Control + S)
                                    <input class="button round red   text-upper" type="reset" name="Reset"
                                           value="Reset"></td>
                                <td>&nbsp; </td>
                                <td>&nbsp; </td>
                            </tr>
                        </table>
                    </form>


                </div>
                <!-- end content-module-main -->


            </div>
            <!-- end content-module -->


        </div>
    </div>
    <!-- end full-width -->

</div>
<!-- end content -->


<!-- FOOTER --><div id="footer">
    <p>Any Queries email to <a href="mailto:jeetsarphare@outlook.com?subject=Print_gallery">jeetsarphare@outlook.com</a>.
    </p>

</div>
<!-- end footer -->

</body>
</html>
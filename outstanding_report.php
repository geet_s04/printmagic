<?php
include_once("init.php");// Use session variable on this page. This function must put on the top of page.
if (!isset($_SESSION['username']) || $_SESSION['usertype'] != 'admin') { // if session variable "username" does not exist.
    header("location: index.php?msg=Please%20login%20to%20access%20admin%20area%20!"); // Re-direct to index.php
} else {
    if (isset($_GET['from_outstanding_date']) && isset($_GET['to_outstanding_date']) && $_GET['from_outstanding_date'] != '' && $_GET['to_outstanding_date'] != '') {

        error_reporting(0);
        $selected_date = $_GET['from_outstanding_date'];
        $selected_date = strtotime($selected_date);
        $mysqldate = date('Y-m-d H:i:s', $selected_date);
        $fromdate = $mysqldate;
        $selected_date = $_GET['to_outstanding_date'];
        $selected_date = strtotime($selected_date);
        $mysqldate = date('Y-m-d H:i:s', $selected_date);

        $todate = $mysqldate;

        ?>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/html4/loose.dtd">
        <html>
        <head>
            <title>Outstanding Report</title>
            <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
            <style type="text/css">
                body {
                font-family: Arial;
                font-size: 14px;
                }
                .report-tbl {
                font-size: 12px;
                }
                .report-tbl td{
                padding: 5px;
                }
            </style>
        </head>
        <style type="text/css" media="print">
            .hide {
                display: none
            }
        </style>
        <script type="text/javascript">
            function printpage() {
                document.getElementById('printButton').style.visibility = "hidden";
                window.print();
                document.getElementById('printButton').style.visibility = "visible";
            }
        </script>
        <body>
        <input name="print" type="button" value="Print" id="printButton" onClick="printpage()">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="center">
                    <div align="left">
                        <?php $line4 = $db->queryUniqueObject("SELECT * FROM store_details ");
                        ?>
                        <strong><?php echo $line4->name; ?></strong><br/>
                        <?php echo $line4->address; ?>,<?php echo $line4->place; ?>, <br/>
                        <?php echo $line4->city; ?>,<?php echo $line4->pin; ?><br/>
                        Website<strong>:<?php echo $line4->web; ?></strong><br>Email<strong>:<?php echo $line4->email; ?></strong><br/>Phone
                        <strong>:<?php echo $line4->phone; ?></strong>
                        <br/>
                        <?php ?>
                    </div>
                    <table width="595" border="0" cellspacing="0" cellpadding="0">

                        <tr>
                            <td height="30" align="center"><strong>Outstanding Report </strong></td>
                        </tr>
                        <tr>
                            <td height="30" align="center">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="right">
                                <table width="300" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="150"><strong>Total Sales </strong></td>
                                        <td width="150">
                                            <?php
												$line1 = "SELECT DISTINCT transactionid FROM stock_sales ";
												$result = mysqli_query($db->connection, $line1);
												$a3=0;
												while ($row = mysqli_fetch_array($result)) {
													
													$item = $db->queryUniqueValue("SELECT grand_total FROM stock_sales WHERE transactionid='".$row['transactionid']."' AND date BETWEEN '$fromdate' AND '$todate' ");
													$a3 = $a3 + $item;
												}
												
												
												?>
                                            Rs.<?php 
											echo  $a3;?></td>
                                            </tr>
                                    <tr>
                                        <td><strong>Received Amount</strong></td>
                                        <?php 
										$paym11 = $db->queryUniqueValue("SELECT sum(payment) FROM transactions where due BETWEEN '$fromdate' AND '$todate' ");
												?>
                                        <td>
                                            Rs.<?php
											if($paym11 == "" || $paym == null)
                                                echo "0";
											else			
											echo $paym11; ?></td>
                                            </tr>
                                    <tr>
                                        <td width="150"><strong>Total OutStanding </strong></td>
                                        <td width="150">Rs.
                                           <?php
											
											echo $bal = $a3 - $paym11; ?></td>
                                            </tr>
                                   </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td width="45">
                                <hr>
                            </td>
                        </tr>
                        <tr>
                            <td height="20">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr><?php $abc = $_GET['from_outstanding_date'];
												$abc = strtotime($abc);
												$mdate = date("d/m/Y", $abc);
												$fdate = $mdate;
												$def = $_GET['to_outstanding_date'];
												$def = strtotime($def);
												$mydate = date("d/m/Y", $def);
										
												$tdate = $mydate;
											?>
                                        <td width="45"><strong>From</strong></td>
                                        <td width="393">&nbsp;<?php echo $fdate; ?></td>
                                        <td width="41"><strong>To</strong></td>
                                        <td width="116">&nbsp;<?php echo $tdate; ?></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td width="45">
                                <hr>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="report-tbl">
                                    <tr>
                                        <td><strong>Customer</strong></td>
										<td><strong>Sales</strong></td>
                                        <td><strong>Payment</strong></td>                                        
                                        <td><strong>Total</strong></td>
                                    </tr>
                                    <?php
									//$result7 = mysqli_query($db->connection, $line1);
										//while ($row7 = mysqli_fetch_array($result7)) {
													
										$item7 = $db->query("SELECT DISTINCT(customer_name) FROM customer_details ORDER BY customer_name ASC");
										
                                    while ($line = $db->fetchNextObject($item7)) {
                                        ?>

                                        <tr>
                                            <td><?php echo $line->customer_name;?></td>
                                            
                                            <?php 
											
											$line10 = "SELECT DISTINCT transactionid FROM stock_sales WHERE customer_id='".$line->customer_name."' AND date BETWEEN '$fromdate' AND '$todate'  ";
											$result10 = mysqli_query($db->connection, $line10);
											$a=0;
											while ($row10 = mysqli_fetch_array($result10)) {
												
												$item = $db->queryUniqueValue("SELECT grand_total FROM stock_sales WHERE transactionid='".$row10['transactionid']."' ");
												$a = $a + $item;
											}
											
											?>
                                            
                                            <td><?php echo $a; ?></td>
                                            <td> <?php echo $paym = $db->queryUniqueValue("SELECT sum(payment) FROM transactions where customer='". $line->customer_name."' AND due BETWEEN '$fromdate' AND '$todate' ");
														if($paym == "")
														{
															echo "0";
															}
											
											 ?> </td>
                                        	<td><?php echo $a - $paym; ?></td>
                                        </tr>


                                        <?php
                                    }
									//}
                                    ?><tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr><tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                            <td width="45" colspan="8">
                                <hr>
                            </td>
                        </tr>
                           <tr>
                        <td></td>
                        <td>Rs.<?php echo $a3; ?></td>
                        <td>Rs.<?php
											if($paym11 == "" || $paym == null)
                                                echo "0";
											else			
											echo $paym11; ?></td>
                        <td><?php echo "Rs.".$bal; ?></td>
                        </tr><tr >
                            <td width="45" colspan="8">
                                <hr>
                            </td>
                        </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        </body>
        </html>
        <?php
    } else
        echo "Please from and to date to process report";
}
?>
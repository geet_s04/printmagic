<?php
include_once("init.php");  // Use session variable on this page. This function must put on the top of page.
if(!isset($_SESSION['username']) || $_SESSION['usertype'] != 'admin'){ // if session variable "username" does not exist.
    header("location: index.php?msg=Please%20login%20to%20access%20admin%20area%20!"); // Re-direct to index.php
}
else
{

error_reporting(0);
if(isset($_GET['sid']))
{
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>Sales Print</title>
	
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="js/date_pic/date_input.css">
    <link rel="stylesheet" href="lib/auto/css/jquery.autocomplete.css">

    <!-- Optimize for mobile devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- jQuery & JS files -->
    <?php include_once("tpl/common_js.php"); ?>
    <script src="js/script.js"></script>
    <style type="text/css" media="print">
        .hide {
            display: none
        }

    </style>
    <script type="text/javascript">
        function printpage(printarea) {
			
			var printContents = document.getElementById(printarea).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
        }
    </script>
    <style type="text/css">
        <!--
        .style1 {
            font-size: 10px
        }

        -->
    </style>
</head>

<body>

<!-- TOP BAR -->
<?php include_once("tpl/top_bar.php"); ?>
<!-- end top-bar -->


<!-- HEADER -->
<div id="header-with-tabs">

    <div class="page-full-width cf">

         <ul id="tabs" class="fl">
            <li><a href="dashboard.php" class="dashboard-tab">Dashboard</a></li>
            <li><a href="view_sales.php" class="active-tab  sales-tab">Sales</a></li>
            <li><a href="view_customers.php" class=" customers-tab">Customers</a></li>
            <li><a href="view_purchase.php" class="purchase-tab">Purchase</a></li>
            <li><a href="view_supplier.php" class=" supplier-tab">Supplier</a></li>
            <li><a href="view_stock_availability.php" class="stock-tab">Stocks / Products</a></li>
            <li><a href="view_payments.php" class="payment-tab">Payments / Outstandings</a></li>
            <li><a href="" class="report-tab">Reports</a></li>
        </ul>
        <!-- end tabs -->

        <!-- end tabs -->

        <!-- Change this image to your own company's logo -->
        <!-- The logo will automatically be resized to 30px height. -->
        <a href="#" id="company-branding-small" class="fr"><img src="<?php if (isset($_SESSION['logo'])) {
                echo "upload/" . $_SESSION['logo'];
            } else {
                echo "upload/posnic.png";
            } ?>" alt="Point of Sale"/></a>

    </div>
    <!-- end full-width -->

</div>
<!-- end header -->


<!-- MAIN CONTENT -->
<div id="content">

    <div class="page-full-width cf">

        <div class="side-menu fl">

            <h3>Sales Management</h3>
            <ul>
                <li><a href="add_sales.php">Add Sales</a></li>
                <li><a href="view_sales.php">View Sales</a></li>
            </ul>

        </div>
        <!-- end side-menu -->

        <div class="side-content fr">

            <div class="content-module1">

                <div class="content-module-heading1 cf">
                
<input name="print" type="button" value="Print" id="printButton" onClick="printpage('printarea')">
				<div id="printarea">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center" valign="top">

            <table width="595" cellspacing="0" cellpadding="0" id="bordertable" border="1">
                <tr>
                    <td align="center"><strong>Sales Receipt <br/>
                        </strong>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="67%" align="left" valign="top">&nbsp;&nbsp;&nbsp;Date: <?php
                                    $sid = $_GET['sid'];
                                    $line = $db->queryUniqueObject("SELECT * FROM stock_sales WHERE transactionid='$sid' ");

                                    $mysqldate = $line->date;

                                    $phpdate = strtotime($mysqldate);

                                    $phpdate = date("d/m/Y", $phpdate);
                                    echo $phpdate;
                                    ?> <br/>
                                    <br/>
                                    <strong><br/>
                                        &nbsp;&nbsp;&nbsp;Receipt No: <?php echo $sid;

                                        ?> </strong><br/></td>
                                <td width="33%">
                                    <div align="center">
                                        <?php $line4 = $db->queryUniqueObject("SELECT * FROM store_details ");
                                        ?>
                                        <strong><?php echo $line4->name; ?></strong><br/>
                                        <?php echo $line4->address; ?>,<?php echo $line4->place; ?>, <br/>
                                        <?php echo $line4->city; ?>,<?php echo $line4->pin; ?><br/>
                                        Website<strong>:<?php echo $line4->web; ?></strong><br>Email<strong>:<?php echo $line4->email; ?></strong><br/>Phone
                                        <strong>:<?php echo $line4->phone; ?></strong>
                                        <br/>
                                        <?php ?>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="90" align="left" valign="top"><br/>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="5%" align="left" valign="top"><strong>&nbsp;&nbsp;TO:</strong></td>
                                <td width="95%" align="left" valign="top"><br/>
                                    <?php
                                    echo $line->customer_id;
                                    $cname = $line->customer_id;

                                    $line2 = $db->queryUniqueObject("SELECT * FROM customer_details WHERE customer_name='$cname' ");

                                    echo "<br>Address:".$line2->customer_address;
                                    ?>
                                    <br/>
                                    <?php
                                    echo "Contact: " . $line2->customer_contact1 . "<br>";
                                    echo "Email : " . $line2->customer_contact2 . "<br>";


                                    ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="12%" align="center" bgcolor="#CCCCCC"><strong>No.</strong></td>
                                <td width="22%" bgcolor="#CCCCCC"><strong>Stock</strong></td>
                                <td width="18%" bgcolor="#CCCCCC"><strong>Quantity</strong></td>
                                <td width="19%" bgcolor="#CCCCCC"><strong>Rate</strong></td>
                                <td width="11%" bgcolor="#CCCCCC">&nbsp;</td>
                                <td width="18%" bgcolor="#CCCCCC"><strong>Total</strong></td>
                            </tr>

                            <tr>
                                <td align="center">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <?php
                            $i = 1;
                            $db->query("SELECT * FROM stock_sales where transactionid='$sid'");
                            while ($line3 = $db->fetchNextObject()) {
                                ?>
                                <tr>
                                    <td align="center"><?php echo $i . "."; ?></td>
                                    <td><?php echo $line3->stock_name; ?></td>
                                    <td><?php echo $line3->quantity; ?></td>
                                    <td><?php echo $line3->selling_price; ?></td>
                                    <td>&nbsp;</td>
                                    <td><?php echo $line3->amount; ?></td>
                                </tr>

                                <?php
                                $i++;
                                $subtotal = $line3->subtotal;
                                $payment = $line3->payment;
                                $balance = $line3->balance;
                                $date = $line->due;
                                $discount = $line3->dis_amount;
                            }
                            ?>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            
                            <tr>
                                <td width="82%" align="right" bgcolor="#CCCCCC"><strong>SubTotal:&nbsp;&nbsp;</strong>
                                </td>
                                <td width="18%" bgcolor="#CCCCCC">Rs.<?php echo $subtotal; ?>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="33%" align="left" valign="top"><br/>
                                    <strong>&nbsp;&nbsp;Paid Amount :&nbsp;&nbsp;Rs.<?php echo $payment; ?><br/>
                                        &nbsp;&nbsp;Balance &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;:&nbsp;&nbsp;Rs.<?php echo $balance; ?><br/>
                                    </strong></td>
                                <td width="67%" align="right"><br/>
                                    <br/>
                                    <br/>
                                    Signature&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" bgcolor="#CCCCCC">Thank you for Business with Us</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</div>
 </div>
            <!-- end content-module -->


        </div>
    </div>
    <!-- end full-width -->

</div>

<!-- FOOTER --><div id="footer">
    <p>Any Queries email to <a href="mailto:jeetsarphare@outlook.com?subject=Print_gallery">jeetsarphare@outlook.com</a>.
    </p>

</div>
<!-- end footer -->

</body>
</html>
<?php
}
else "Error in processing printing the sales receipt";
}
?>
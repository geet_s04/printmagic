<?php
include_once("init.php");

?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Add Payment</title>

    <!-- Stylesheets -->

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="js/date_pic/date_input.css">
    <link rel="stylesheet" href="lib/auto/css/jquery.autocomplete.css">
    <!-- Optimize for mobile devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- jQuery & JS files -->
    <?php include_once("tpl/common_js.php"); ?>
    <script src="js/date_pic/jquery.date_input.js"></script>
    <script src="js/script.js"></script>
    <script src="lib/auto/js/jquery.autocomplete.js "></script>
    <script type="text/javascript">
        $(function () {

            $("#supplier").autocomplete("supplier1.php", {
                width: 160,
                autoFill: true,
                selectFirst: true
            });
            
        });

    </script>
    <script>
        $(document).ready(function () {
            $('#test1').jdPicker();
			 
            // validate signup form on keyup and submit
            $("#form1").validate({
                rules: {
                    name: {
                        required: true,
                        minlength: 3,
                        maxlength: 200
                    },

                    cost: {
                        required: true

                    },
                    new_payment: {
                        required: true
                    },

                    paid: {
                        required: true

                    },
                    sell: {
                        required: true

                    }
                },
                messages: {
                    name: {
                        required: "Please enter a Stock Name",
                        minlength: "Stock must consist of at least 3 characters"
                    },
                    cost: {
                        required: "Please enter a cost Price"
                    },

                    new_payment: {
                        required: "Please enter a New Payment"
                    },
                    paid: {
                        required: "Please enter a Paid Amount"
                    },
                    sell: {
                        required: "Please enter a Sell Price"
                    }
                }
            });

        });
		
        function numbersonly(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
            if (unicode != 8 && unicode != 46 && unicode != 37 && unicode != 38 && unicode != 39 && unicode != 40) { //if the key isn't the backspace key (which we should allow)
                if (unicode < 48 || unicode > 57)
                    return false
            }
        }
        function change_balance() {
            if (parseFloat(document.getElementById('new_payment').value) > parseFloat(document.getElementById('balance').value)) {
                document.getElementById('new_payment').value = parseFloat(document.getElementById('balance').value);
            }
        }
    </script>

</head>
<body>

<!-- TOP BAR -->
<?php include_once("tpl/top_bar.php"); ?>
<!-- end top-bar -->


<!-- HEADER -->
<div id="header-with-tabs">

    <div class="page-full-width cf">
		<ul id="tabs" class="fl">
           
            <li><a href="dashboard.php" class="dashboard-tab">Dashboard</a></li>
            <li><a href="view_sales.php" class="sales-tab">Sales</a></li>
            <li><a href="view_customers.php" class="customers-tab">Customers</a></li>
            <li><a href="view_purchase.php" class="purchase-tab">Purchase</a></li>
            <li><a href="view_supplier.php" class=" supplier-tab">Supplier</a></li>
            <li><a href="view_stock_availability.php" class="stock-tab">Stocks / Products</a></li>
            <li><a href="view_payments.php" class=" active-tab payment-tab">Payments / Outstandings</a></li>
            <li><a href="view_report.php" class="report-tab">Reports</a></li>
        </ul>
        <!-- end tabs -->

        <!-- Change this image to your own company's logo -->
        <!-- The logo will automatically be resized to 30px height. -->
        <a href="#" id="company-branding-small" class="fr"><img src="<?php if (isset($_SESSION['logo'])) {
                echo "upload/" . $_SESSION['logo'];
            } else {
                echo "upload/posnic.png";
            } ?>" alt="Point of Sale"/></a>

    </div>
    <!-- end full-width -->

</div>
<!-- end header -->


<!-- MAIN CONTENT -->
<div id="content">

    <div class="page-full-width cf">

        <div class="side-menu fl">

            <h3>Payment</h3>
            <ul>
                <li><a href="view_payments.php">Payments</a></li>
                <li><a href="view_out_standing.php">Out standings</a></li>
                <li><a href="purchase_payments.php">Purchase Payments</a></li>
            </ul>
            <div style="width: auto;height: 300px;background: #ffffff">
                <br><br>
                
                <br><br>
            </div>
        </div>
        <!-- end side-menu -->

        <div class="side-content fr">

            <div class="content-module">

                <div class="content-module-heading cf">

                    <h3 class="fl">Update Payment</h3>
                    <span class="fr expand-collapse-text">Click to collapse</span>
                    <span class="fr expand-collapse-text initial-expand">Click to expand</span>

                </div>
                <!-- end content-module-heading -->

                <div class="content-module-main cf">
                    <form name="form1" method="post" id="form1" action="">

                        <table class="form" border="0" cellspacing="0" cellpadding="0">
                            <?php
                            if (isset($_POST['id'])) {
                                $id = mysqli_real_escape_string($db->connection, $_POST['id']);
                                $customer = mysqli_real_escape_string($db->connection, $_POST['supplier']);
                                $subtotal = mysqli_real_escape_string($db->connection, $_POST['total']);
                                $mode = mysqli_real_escape_string($db->connection, $_POST['mode']);
                                $selected_date = $_POST['date'];
                                $selected_date = strtotime($selected_date);
                                $mysqldate = date('Y-m-d', $selected_date);
                                $due = $mysqldate;
                                
                                $max = $db->maxOfAll("id", "purchase_payment");
                                $receiptid = "RCPT" . $max;
                                $db->query("INSERT INTO purchase_payment(type,customer,payment,rid,mode,due,receiptid) values('purchase','$customer','$subtotal','$id','$mode','$due','$receiptid')");
                                    
                                    echo "<br><font color=green size=+1 > [ $id ] New Payment Added!</font>";
                               
                            }

                            ?>
                            
                            <form name="form1" method="post" id="form1" action="">

                                
                                <tr><?php
                                $max = $db->maxOfAll("id", "purchase_payment");
                                $max = $max + 1;
                                $autoid = "PUR" . $max . "";
                                ?>
                                <input name="id" type="hidden" value="<?php echo $autoid ?> ">
                                    <td>&nbsp;</td>
                                    <td><input name="stock_id" type="hidden" readonly readonly="readonly"
                                               id="stockid" maxlength="200" class="round default-width-input"
                                               value="<?php echo $autoid ?> "/>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Date</td>
                                    <td><input name="date" type="text" id="test1" maxlength="20"
                                               class="round default-width-input"
                                               value="<?php echo date("Y/m/d") ?>"/></td>
                                    
                                    <td>Suppier</td>
                                    <td><input name="supplier" type="text" id="supplier" maxlength="200"
                                                class="round default-width-input" placeholder="Name"
                                               value=""/></td>
                                    
                                </tr>

                                
                                <tr>
                                    <td>Mode &nbsp;</td>
                                <td>
                                    <input name="mode" type="text" id="mode" maxlength="200"
                                                class="round default-width-input" placeholder="CASH/CHEQUE"
                                               value=""/>
                                </td>
                                    <td>Payment</td>
                                    <td><input name="total" type="text" id="total" maxlength="20" 
                                               class="round default-width-input"
                                               value=""/></td>
                                </tr>
								

                                <tr>
                                    <td>
                                        <input class="button round blue image-right ic-add text-upper" type="submit"
                                               name="Submit" value="Save">
                                        
                                    </td>
                                    <td align="right"><input class="button round red   text-upper" type="reset"
                                                             name="Reset" value="Reset">
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                        </table>
                    </form>


                </div>
                <!-- end content-module-main -->


            </div>
            <!-- end content-module -->


        </div>
        <!-- end full-width -->

    </div>
    <!-- end content -->

 <!-- FOOTER --><div id="footer">
    <p>Any Queries email to <a href="mailto:jeetsarphare@outlook.com?subject=Print_gallery">jeetsarphare@outlook.com</a>.
    </p>

</div>
<!-- end footer -->

</body>
</html>
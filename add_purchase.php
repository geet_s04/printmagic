<?php
include_once("init.php");

?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Add Sales</title>

    <!-- Stylesheets -->

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="js/date_pic/date_input.css">
    <link rel="stylesheet" href="lib/auto/css/jquery.autocomplete.css">

    <!-- Optimize for mobile devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- jQuery & JS files -->
    <?php include_once("tpl/common_js.php"); ?>
    <script src="js/script.js"></script>
    <script src="js/date_pic/jquery.date_input.js"></script>
    <script src="lib/auto/js/jquery.autocomplete.js "></script>
<script>
function nw() {
    document.getElementById("guid").innerHTML = Math.floor((Math.random() * 100) + 1);
}
</script>
    <script type="text/javascript">
        $(function () {

            $("#supplier").autocomplete("supplier1.php", {
                width: 160,
                autoFill: true,
                selectFirst: true
            });
            
            $("#supplier").blur(function () {


                $.post('check_supplier_details.php', {stock_name1: $(this).val()},
                    function (data) {

                        $("#address").val(data.address);
                        $("#contact1").val(data.contact1);

                        if (data.address != undefined)
                            $("#0").focus();

                    }, 'json');


            });
            $('#test1').jdPicker({
			});


            var hauteur = 0;
            $('.code').each(function () {
                if ($(this).height() > hauteur) hauteur = $(this).height();
            });

            $('.code').each(function () {
                $(this).height(hauteur);
            });
        });

    </script>
    <script>
function nw() {
    document.getElementById("guid").value = Math.floor((Math.random() * 100) + 1);
}
</script>
    <script>
        /*$.validator.setDefaults({
         submitHandler: function() { alert("submitted!"); }
         });*/
        $(document).ready(function () {
            document.getElementById('bill_no').focus();
            // validate signup form on keyup and submit
            $("#form1").validate({
                rules: {
                    bill_no: {
                        required: false,
                        minlength: 3,
                        maxlength: 200
                    },
                    stockid: {
                        required: true
                    },
                    grand_total: {
                        required: true
                    },
                    supplier: {
                        required: true,
                    },
                    payment: {
                        required: true,
                    }
                },
                messages: {
                    supplier: {
                        required: "Please Enter Supplier"
                    },
                    stockid: {
                        required: "Please Enter Stock ID"
                    },
                    payment: {
                        required: "Please Enter Payment"
                    },
                    grand_total: {
                        required: "Add Stock Items"
                    },
                    bill_no: {
                        required: "Please Enter Bill Number",
                        minlength: "Bill Number must consist of at least 3 characters"
                    }
                }
            });

        });
        function numbersonly(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
            if (unicode != 8 && unicode != 46 && unicode != 37 && unicode != 27 && unicode != 38 && unicode != 39 && unicode != 40 && unicode != 9) { //if the key isn't the backspace key (which we should allow)
                if (unicode < 48 || unicode > 57)
                    return false
            }
        }


    </script>
    <script type="text/javascript">
        function remove_row(o) {
            var p = o.parentNode.parentNode;
            p.parentNode.removeChild(p);
        }
		function urlencode( str ) 
{
   var ret = str; 
   ret = ret.toString();
   ret = encodeURIComponent(ret);
   ret = ret.replace(/%20/g, '_');
   return ret;
}
        function add_values() {
            if (unique_check()) {

                if (document.getElementById('edit_guid').value == "") {
                    if (document.getElementById('item').value != "" && document.getElementById('quty').value != "" && document.getElementById('total').value != "") {

                        if (document.getElementById('quty').value != 0) {
                            code = document.getElementById('item').value;
							
                            quty = document.getElementById('quty').value;
                            sell = document.getElementById('sell').value;
							var rem = document.getElementById('rem').value;
							rem=urlencode(rem);
                            
                            total = document.getElementById('total').value;
                            item = document.getElementById('guid').value;
                            main_total = document.getElementById('posnic_total').value;

                            $('<tr id=' + item + '><td><input type=hidden value=' + code + ' id=' + item + 'id ><input type=text name="stock_name[]" value=' + code + ' id=' + item + 'st style="width: 150px" readonly="readonly" class="round  my_with" ></td><td><input type=text name=quty[] readonly="readonly" value=' + quty + ' id=' + item + 'q class="round  my_with" style="text-align:right;" ></td><td><input type=text name=rem[] readonly="readonly" value=' + rem + ' id=' + item + 'q1 class="round  my_with" style="text-align:right;" ></td><td><input type=text name=sell[] readonly="readonly" value=' + sell + ' id=' + item + 's class="round  my_with" style="text-align:right;"  ></td><td><input type=text name=jibi[] readonly="readonly" value=' + total + ' id=' + item + 'to class="round  my_with" style="width: 120px;margin-left:20px;text-align:right;" ><input type=hidden name=total[] id=' + item + 'my_tot value=' + main_total + '> </td><td><input type=button value="" id=' + item + ' style="width:30px;border:none;height:30px;background:url(images/edit_new.png)" class="round" onclick="edit_stock_details(this.id)"  ></td><td><input type=button value="" id=' + item + ' style="width:30px;border:none;height:30px;background:url(images/close_new.png)" class="round" onclick=reduce_balance("' + item + '");$(this).closest("tr").remove(); ></td></tr>').fadeIn("slow").appendTo('#item_copy_final');
							
							document.getElementById('quty').value = "";
                            document.getElementById('rem').value = "";
                            document.getElementById('sell').value = "";
                            document.getElementById('total').value = "";
                            document.getElementById('item').value = "";
                            document.getElementById('guid').value = "";
                            if (document.getElementById('grand_total').value == "") {
                                document.getElementById('grand_total').value = main_total;
                            } else {
                                document.getElementById('grand_total').value = parseFloat(document.getElementById('grand_total').value) + parseFloat(main_total);
                            }
                            document.getElementById('main_grand_total').value = parseFloat(document.getElementById('grand_total').value);
                            document.getElementById(item + 'st').value = code;
                            document.getElementById(item + 'to').value = total;
                        } else {
                            alert('No Stock Available For This Item');
                        }
                    } else {
                        alert('Please Select An Item');
                    }
                } else {
                    id = document.getElementById('edit_guid').value;
                    document.getElementById(id + 'st').value = document.getElementById('item').value;
					document.getElementById(id + 'q').value = document.getElementById('quty').value;
					document.getElementById(id + 'q1').value = document.getElementById('rem').value;
                    document.getElementById(id + 's').value = document.getElementById('sell').value;
                    document.getElementById('grand_total').value = parseFloat(document.getElementById('grand_total').value) + parseFloat(document.getElementById('posnic_total').value) - parseFloat(document.getElementById(id + 'my_tot').value);
                    document.getElementById('main_grand_total').value = parseFloat(document.getElementById('grand_total').value);
                    document.getElementById(id + 'to').value = document.getElementById('total').value;
                    document.getElementById(id + 'id').value = id;

                    document.getElementById(id + 'my_tot').value = document.getElementById('posnic_total').value
                    
					document.getElementById('quty').value = "";
                    document.getElementById('rem').value = "";
                    document.getElementById('sell').value = "";
                    document.getElementById('total').value = "";
                    document.getElementById('item').value = "";
                    document.getElementById('guid').value = "";
                    document.getElementById('edit_guid').value = "";
                }
            }
            discount_amount();
        }
        function total_amount() {
            balance_amount();

            document.getElementById('total').value = document.getElementById('sell').value 
            document.getElementById('posnic_total').value = document.getElementById('total').value;
            //  document.getElementById('total').value = '$ ' + parseFloat(document.getElementById('total').value).toFixed(2);
            if (document.getElementById('item').value === "") {
                document.getElementById('item').focus();
            }
        }
		 
        function edit_stock_details(id) {
            document.getElementById('item').value = document.getElementById(id + 'st').value;
			
            document.getElementById('quty').value = document.getElementById(id + 'q').value;
            document.getElementById('rem').value = document.getElementById(id + 'q1').value;
            document.getElementById('sell').value = document.getElementById(id + 's').value;
            document.getElementById('total').value = document.getElementById(id + 'to').value;

            document.getElementById('guid').value = id;
            document.getElementById('edit_guid').value = id;

        }
        function unique_check() {
            if (!document.getElementById(document.getElementById('guid').value) || document.getElementById('edit_guid').value == document.getElementById('guid').value) {
                return true;

            } else {

                alert("This Item is already added In This Purchase");
                document.getElementById('item').focus();
				
                document.getElementById('sell').value = "";
                document.getElementById('total').value = "";
                document.getElementById('item').value = "";
                document.getElementById('guid').value = "";
                document.getElementById('edit_guid').value = "";
                return false;
            }
        }
        function quantity_chnage(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
            if (unicode != 13 && unicode != 9) {
            }
            else {
                add_values();

                document.getElementById("item").focus();

            }
            if (unicode != 27) {
            }
            else {

                document.getElementById("item").focus();
            }
        }
        function formatCurrency(fieldObj) {
            if (isNaN(fieldObj.value)) {
                return false;
            }
            fieldObj.value = '$ ' + parseFloat(fieldObj.value).toFixed(2);
            return true;
        }
		function discount_amount() {

            document.getElementById('payable_amount').value = parseFloat(document.getElementById('grand_total').value) ;
            if (parseFloat(document.getElementById('payment').value) > parseFloat(document.getElementById('payable_amount').value)) {
                document.getElementById('payment').value = parseFloat(document.getElementById('payable_amount').value);

            }

        }
        function balance_amount() {
            if (document.getElementById('payable_amount').value != "" && document.getElementById('payment').value != "") {
                data = parseFloat(document.getElementById('payable_amount').value);
                document.getElementById('balance').value = data - parseFloat(document.getElementById('payment').value);
                if (parseFloat(document.getElementById('payable_amount').value) >= parseFloat(document.getElementById('payment').value)) {

                } else {
                    if (document.getElementById('payable_amount').value != "") {
                        document.getElementById('balance').value = '000.00';
                        document.getElementById('payment').value = parseFloat(document.getElementById('payable_amount').value);
                    } else {
                        document.getElementById('balance').value = '000.00';
                        document.getElementById('payment').value = "";
                    }
                }
            } else {
                document.getElementById('balance').value = "";
            }


        }
        
        function reduce_balance(id) {
            var minus = parseFloat(document.getElementById(id + "my_tot").value);
            document.getElementById('grand_total').value = parseFloat(document.getElementById('grand_total').value) - minus;
            document.getElementById('main_grand_total').value = parseFloat(document.getElementById('grand_total').value);
            discount_amount();
            //console.log(id);
        }
       
    </script>

</head>
<body>

<!-- TOP BAR -->
<?php include_once("tpl/top_bar.php"); ?>
<!-- end top-bar -->


<!-- HEADER -->
<div id="header-with-tabs">

    <div class="page-full-width cf">

        <ul id="tabs" class="fl">
            <li><a href="dashboard.php" class="dashboard-tab">Dashboard</a></li>
            <li><a href="view_sales.php" class="sales-tab">Sales</a></li>
            <li><a href="view_customers.php" class=" customers-tab">Customers</a></li>
            <li><a href="view_purchase.php" class="active-tab purchase-tab">Purchase</a></li>
            <li><a href="view_supplier.php" class=" supplier-tab">Supplier</a></li>
            <li><a href="view_stock_availability.php" class="stock-tab">Stocks / Products</a></li>
            <li><a href="view_payments.php" class="payment-tab">Payments / Outstandings</a></li>
            <li><a href="view_report.php" class="report-tab">Reports</a></li>
        </ul>
        <!-- end tabs -->

        <!-- Change this image to your own company's logo -->
        <!-- The logo will automatically be resized to 30px height. -->
        <a href="#" id="company-branding-small" class="fr"><img src="<?php if (isset($_SESSION['logo'])) {
                echo "upload/" . $_SESSION['logo'];
            } else {
                echo "upload/posnic.png";
            } ?>" alt="Point of Sale"/></a>

    </div>
    <!-- end full-width -->

</div>
<!-- end header -->


<!-- MAIN CONTENT -->
<div id="content">

    <div class="page-full-width cf">

        <div class="side-menu fl">

            <h3>Stock Purchase</h3>
            <ul>
                <li><a href="add_purchase.php">Add Purchase</a></li>
                <li><a href="view_purchase.php">View Purchase </a></li>
                <li><a href="add_expense.php">Add Expenses </a></li>
                <li><a href="view_expense.php">View Expenses </a></li>


            </ul>

        </div>
        <!-- end side-menu -->

        <div class="side-content fr">

            <div class="content-module">

                <div class="content-module-heading cf">

                    <h3 class="fl">Add Purchase</h3>
                    <span class="fr expand-collapse-text">Click to collapse</span>
                    <span class="fr expand-collapse-text initial-expand">Click to expand</span>

                </div>
                <!-- end content-module-heading -->

                <div class="content-module-main cf">


                   
					<?php
                    //Gump is libarary for Validatoin
                    if (isset($_GET['msg'])) {
                        echo $_GET['msg'];
                    }
                    if (isset($_POST['supplier']) and isset($_POST['stock_name'])) {
                        $_POST = $gump->sanitize($_POST);
                        $gump->validation_rules(array(
                            'supplier' => 'required|max_len,100|min_len,3'


                        ));

                        $gump->filter_rules(array(
                            'supplier' => 'trim|sanitize_string|mysqli_escape'


                        ));

                        $validated_data = $gump->run($_POST);
                        $supplier = "";
                        $stockid = "";
                        $stock_name = "";
                        $cost = "";
                        $bill_no = "";


                        if ($validated_data === false) {
                            echo $gump->get_readable_errors(true);
                        } else {
                            $username = $_SESSION['username'];

                            $stockid = mysqli_real_escape_string($db->connection, $_POST['stockid']);

                            $bill_no = mysqli_real_escape_string($db->connection, $_POST['bill_no']);
                            $supplier = mysqli_real_escape_string($db->connection, $_POST['supplier']);
                            $address = mysqli_real_escape_string($db->connection, $_POST['address']);
                            $contact = mysqli_real_escape_string($db->connection, $_POST['contact']);
                            $stock_name = $_POST['stock_name'];

                            $count = $db->countOf("supplier_details", "supplier_name='$supplier'");
                            if ($count == 0) {
                                $db->query("insert into supplier_details(supplier_name,supplier_address,supplier_contact1) values('$supplier','$address','$contact')");
                            }
                            $quty = $_POST['quty'];
                            $date = date("d M Y h:i A");
							
                            $cost = $_POST['sell'];
                            $total = $_POST['total'];
                            $subtotal = $_POST['subtotal'];
                            $remark = $_POST['rem'];
                            $payment = mysqli_real_escape_string($db->connection, $_POST['payment']);
                            $balance = mysqli_real_escape_string($db->connection, $_POST['balance']);
							
							
                            $temp_balance = $db->queryUniqueValue("SELECT balance FROM supplier_details WHERE supplier_name='$supplier'");
                            $temp_balance = (int)$temp_balance + (int)$balance;
                            $db->execute("UPDATE supplier_details SET balance=$temp_balance WHERE supplier_name='$supplier'");
                            

                            $autoid = $_POST['stockid'];
                            $autoid1 = $autoid;
                            $selected_date = $_POST['date'];
                            $selected_date = strtotime($selected_date);
                            $date = date('Y-m-d H:i:s', $selected_date);
                            for ($i = 0; $i < count($stock_name); $i++) {
								 $stock_name1 = $_POST['stock_name'][$i];
								 $quty1 = $_POST['quty'][$i];
								 $remark1 = $_POST['rem'][$i];
								 $total1 = $_POST['total'][$i];


                                   $db->query("insert into purchase_detail(purchase_id,date,billno,supplier_id,item,quantity,remark,total,payment,balance,subtotal,count) values('$autoid1','$date','$bill_no','$supplier','$stock_name1','$quty1','$remark1','$total1','$payment','$balance','$subtotal',$i+1)");
                                   
                            }
							$msg = "<br><font color=green size=6px >Purchase Added successfully Ref: [" . $_POST['stockid'] . "] !</font>";
							
                            echo "<script>window.location = 'add_purchase.php?msg=$msg';</script>";
                        }

                    }

                    ?>
                    
                    <form name="form1" method="post" id="form1" action="">
                        <input type="hidden" id="posnic_total">
						<?php if (isset($_GET['msg'])) {
                        echo $_GET['msg'];
                    } ?>
                        <p><strong>Add Stock/Product </strong> - Add New ( Control +2)</p>
                        <table class="form" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <?php
                                $max = $db->maxOfAll("id", "purchase_detail");
                                $max = $max + 3;
                                $autoid = "PUR" . $max . "";
                                ?>
                                <td>Stock ID:</td>
                                <td><input name="stockid" type="text" id="stockid" readonly maxlength="200"
                                           class="round default-width-input" style="width:130px "
                                           value="<?php echo $autoid ?>"/></td>

                                <td>Date:</td>
                                <td><input name="date" id="test1" placeholder="" value="<?php echo date('d-m-Y'); ?>"
                                           type="text" id="name" maxlength="200" class="round default-width-input"/>
                                </td>
                                <td><span class="man">*</span>Bill No:</td>
                                <td><input name="bill_no" placeholder="ENTER BILL NO" type="text" id="bill_no"
                                           maxlength="200" class="round default-width-input" style="width:120px "/></td>

                            </tr>
                            <tr>
                                <td><span class="man">*</span>Supplier:</td>
                                <td><input name="supplier" placeholder="ENTER CUSTOMER" type="text" id="supplier"
                                           maxlength="200" class="round default-width-input" style="width:130px "/></td>

                                <td>Address:</td>
                                <td><input name="address" placeholder="ENTER ADDRESS" type="text" id="address"
                                           maxlength="200" class="round default-width-input"/></td>

                                <td>contact:</td>
                                <td><input name="contact" placeholder="ENTER CONTACT" type="text" id="contact1"
                                           maxlength="200" class="round default-width-input"
                                           onkeypress="return numbersonly(event)" style="width:120px "/></td>

                            </tr>
                        </table>
                        <input type="hidden" id="guid">
                        <input type="hidden" id="edit_guid">
                        <table class="form">
                            <tr>
                                <td>Item:</td>
                                <td>Quantity:</td>
                                <td>Remark:</td>
                                <td>Cost:</td>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total</td>
                                <td>&nbsp; </td>
                            </tr>
                            <tr>

                                <td><input name="" type="text" id="item" maxlength="200" onKeyPress="nw();"
                                           class="round default-width-input " style="width: 150px"/></td>
								
                                <td><input name="" type="text" id="quty" maxlength="200"
                                           class="round default-width-input my_with"
                                           onkeyup="unique_check();"/></td>
								
                               <td><input name="" type="text" id="rem" maxlength="200"
                                           class="round default-width-input " style="width: 150px"/></td>
                                           
                                <td><input name="" type="text" id="sell" maxlength="200"
                                           class="round default-width-input my_with" onkeyup="total_amount()";/></td>
                                           
                                <td><input name="" type="hidden" id="total" maxlength="200"
                                           class="round default-width-input " style="width:120px;  margin-left: 20px"/>
                                </td>
                                <td><input type="button" onclick="add_values()" onkeyup=" balance_amount();"
                                           id="add_new_code"
                                           style="margin-left:30px; width:30px;height:30px;border:none;background:url(images/add_new.png)"
                                           class="round"></td>

                            </tr>
                        </table>
                        <div style="overflow:auto ;max-height:300px;  ">
                            <table class="form" id="item_copy_final">

                            </table>
                        </div>


                        <table class="form">
                            
                            <tr>
                                <td>&nbsp; </td>
                                
                                <td>Grand Total:<input type="hidden" readonly id="grand_total"
                                                       name="subtotal">
                                    <input type="text" id="main_grand_total" readonly
                                           class="round default-width-input" style="text-align:right;width: 120px">
                                </td>
                                <td>&nbsp; </td>
                                
                            </tr>
                            <tr>
                                <td>&nbsp; </td>
                                <td><input type="hidden" class="round" onkeyup=" balance_amount(); "
                                                   onkeypress="return numbersonly(event);" name="payment" id="payment" value="">
                                </td>

                                <td><input type="hidden" class="round" readonly id="balance"
                                                   name="balance">
                                </td>
                                <td>&nbsp; </td>

                                <td>&nbsp; </td>
                                <td>&nbsp; </td>
                                <td>Payable Amount:<input type="hidden" readonly id="grand_total">
                                    <input type="text" id="payable_amount" readonly name="payable"
                                           class="round default-width-input" style="text-align:right;width: 120px">
                                </td>
                                <td>&nbsp; </td>
                                <td>&nbsp; </td>
                                <td>&nbsp; </td>
                            </tr>
                        </table>
                        
                        <table class="form">
                            <tr>
                                <td>
                                    <input class="button round blue image-right ic-add text-upper" type="submit"
                                           name="Submit" value="Add" >
                                </td>
                                <td> (Control + S)
                                    <input class="button round red   text-upper" type="reset" name="Reset"
                                           value="Reset"></td>
                                <td>&nbsp; </td>
                                <td>&nbsp; </td>
                            </tr>
                        </table>
                    </form>


                </div>
                <!-- end content-module-main -->


            </div>
            <!-- end content-module -->


        </div>
    </div>
    <!-- end full-width -->

</div>
<!-- end content -->


<!-- FOOTER --><div id="footer">
    <p>Any Queries email to <a href="mailto:jeetsarphare@outlook.com?subject=Print_gallery">jeetsarphare@outlook.com</a>.
    </p>

</div>
<!-- end footer -->

</body>
</html>
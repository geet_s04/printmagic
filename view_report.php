<?php
include_once("init.php");

?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Report</title>

    <!-- Stylesheets -->

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="js/date_pic/date_input.css">

    <!-- Optimize for mobile devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- jQuery & JS files -->
    <?php include_once("tpl/common_js.php"); ?>
    <script src="js/date_pic/jquery.date_input.js"></script>
    <script src="js/script.js"></script>
    <script>
        /*$.validator.setDefaults({
         submitHandler: function() { alert("submitted!"); }
         });*/
        $(document).ready(function () {
            $('#from_sales_date').jdPicker();
            $('#to_sales_date').jdPicker();
			$('#from_sales_date1').jdPicker();
            $('#to_sales_date1').jdPicker();
			$('#from_sales_date21').jdPicker();
            $('#to_sales_date21').jdPicker();
			$('#from_outstanding_date').jdPicker();
            $('#to_outstanding_date').jdPicker();
			
            $('#from_purchase_date').jdPicker();
            $('#to_purchase_date').jdPicker();
            $('#from_sales_purchase_date').jdPicker();
            $('#to_sales_purchase_date').jdPicker();
            // validate signup form on keyup and submit
            $("#form1").validate({
                rules: {
                    name: {
                        required: true,
                        minlength: 3,
                        maxlength: 200
                    },

                    cost: {
                        required: true

                    },
                    sell: {
                        required: true

                    }
                },
                messages: {
                    name: {
                        required: "Please enter a Stock Name",
                        minlength: "Stock must consist of at least 3 characters"
                    },
                    cost: {
                        required: "Please enter a cost Price"
                    },
                    sell: {
                        required: "Please enter a Sell Price"
                    }
                }
            });

        });
        function numbersonly(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
            if (unicode != 8 && unicode != 46 && unicode != 37 && unicode != 38 && unicode != 39 && unicode != 40) { //if the key isn't the backspace key (which we should allow)
                if (unicode < 48 || unicode > 57)
                    return false
            }
        }
        function change_balance() {
            if (parseFloat(document.getElementById('new_payment').value) > parseFloat(document.getElementById('balance').value)) {
                document.getElementById('new_payment').value = parseFloat(document.getElementById('balance').value);
            }
        }

        function sales_report_fn() {
            window.open("dompdf/sales.php?from_sales_date=" + $('#from_sales_date').val() + "&to_sales_date=" + $('#to_sales_date').val(), "myNewWinsr", "width=620,height=800,toolbar=0,menubar=no,status=no,resizable=yes,location=no,directories=no,scrollbars=yes");

        }
        function sales1_report_fn() {
            window.open("sales_report.php?from_sales_date=" + $('#from_sales_date').val() + "&to_sales_date=" + $('#to_sales_date').val(), "myNewWinsr", "width=620,height=800,toolbar=0,menubar=no,status=no,resizable=yes,location=no,directories=no,scrollbars=yes");

        }
		function sales_report_fn1() {
            window.open("dompdf/customer.php?from_sales_date1=" + $('#from_sales_date1').val() + "&to_sales_date1=" + $('#to_sales_date1').val() +
			"&selectcust=" + $('#selectcust').val() , "myNewWinsr", "width=620,height=800,toolbar=0,menubar=no,status=no,resizable=yes,location=no,directories=no,scrollbars=yes");

        }
		function new_customer_report11() {
            window.open("dompdf/new_customer.php?from_sales_date21=" + $('#from_sales_date21').val() + "&to_sales_date21=" + $('#to_sales_date21').val() +
			"&selectcust21=" + $('#selectcust21').val() , "myNewWinsr", "width=620,height=800,toolbar=0,menubar=no,status=no,resizable=yes,location=no,directories=no,scrollbars=yes");

        }
        function sales1_report_fn1() {
            window.open("customer_report.php?from_sales_date1=" + $('#from_sales_date1').val() + "&to_sales_date1=" + $('#to_sales_date1').val() +
			"&selectcust=" + $('#selectcust').val() , "myNewWinsr", "width=620,height=800,toolbar=0,menubar=no,status=no,resizable=yes,location=no,directories=no,scrollbars=yes");

        }
		function sales_report_fn12() {
            window.open("dompdf/new.php?from_out_date1=" + $('#from_out_date1').val() + "&selectcustom=" + $('#selectcustom').val() , "myNewWinsr", "width=620,height=800,toolbar=0,menubar=no,status=no,resizable=yes,location=no,directories=no,scrollbars=yes");

        }
        function sales1_report_fn12() {
            window.open("customer_outstanding.php?from_out_date1=" + $('#from_out_date1').val() + "&selectcustom=" + $('#selectcustom').val() , "myNewWinsr", "width=620,height=800,toolbar=0,menubar=no,status=no,resizable=yes,location=no,directories=no,scrollbars=yes");

        }
        function purchase_report_fn() {
            window.open("dompdf/purchase.php?from_purchase_date=" + $('#from_purchase_date').val() + "&to_purchase_date=" + $('#to_purchase_date').val(), "myNewWinsr", "width=620,height=800,toolbar=0,menubar=no,status=no,resizable=yes,location=no,directories=no,scrollbars=yes");

        }
		
	function outstanding_report_fn() {
            window.open("outstanding_report.php?from_outstanding_date=" + $('#from_outstanding_date').val() + "&to_outstanding_date=" + $('#to_outstanding_date').val(), "myNewWinsr", "width=620,height=800,toolbar=0,menubar=no,status=no,resizable=yes,location=no,directories=no,scrollbars=yes");

        }
        function sales_purchase_report_fn() {
            window.open("all_report.php?from_sales_purchase_date=" + $('#from_sales_purchase_date').val() + "&to_sales_purchase_date=" + $('#to_sales_purchase_date').val(), "myNewWinsr", "width=620,height=800,toolbar=0,menubar=no,status=no,resizable=yes,location=no,directories=no,scrollbars=yes");

        }

        function stock_sales_report_fn() {
            window.open("sales_stock_report.php?from_stock_sales_date=" + $('#from_stock_sales_date').val() + "&to_stock_sales_date=" + $('#to_stock_sales_date').val(), "myNewWinsr", "width=620,height=800,toolbar=0,menubar=no,status=no,resizable=yes,location=no,directories=no,scrollbars=yes");

        }
    </script>

</head>
<body>

<!-- TOP BAR -->
<?php include_once("tpl/top_bar.php"); ?>
<!-- end top-bar -->


<!-- HEADER -->
<?php include_once("tpl/header.php"); ?>
<!-- end header -->


<!-- MAIN CONTENT -->
<div id="content">

    <div class="page-full-width cf">

        
        <div class="side-content fr">

            <div class="content-module">

                <div class="content-module-heading cf">

                    <h3 class="fl">Report</h3>
                    <span class="fr expand-collapse-text">Click to collapse</span>
                    <span class="fr expand-collapse-text initial-expand">Click to expand</span>

                </div>
                <!-- end content-module-heading -->

                <div class="content-module-main cf">
                    <!-- <form action=""> -->

                        <table class="form report-view" border="0" cellspacing="0" cellpadding="0">
                            <form action="sales_report.php" method="post" name="form1" id="form1" name="sales_report"
                                  id="sales_report" target="myNewWinsr">
                                <tr>

                                    <td style="width: 10%;"><strong>Sales Report </strong></td>
                                    <td style="width: 10%;text-align: right;padding-right: 10px;">From</td>
                                    <td style="width: 10%;">
                                   <input class="form-control" name="from_sales_date" type="text" id="from_sales_date"
                                               style="width:80px;"></td> 
                                    <td style="width: 10%;text-align: right;padding-right: 10px;">To</td>
                                    <td style="width: 10%;"><input class="form-control"  name="to_sales_date" type="text" id="to_sales_date" style="width:80px;">
                                    </td>
                                    <td style="width: 10%;"><input class="btn btn-block btn-info" name="submit" type="button" value="Show" onClick='sales1_report_fn();'>
									<!--<input name="submit" type="button" value="Download" onClick='sales_report_fn();'> -->
                                    </td>

                                </tr>
                            </form>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>

                           <!-- <form action="purchase_report.php" method="post" name="purchase_report" target="_blank">
                                <tr>
                                    <td><strong>Purchase Report </strong></td>
                                    <td>From</td>
                                    <td><input name="from_purchase_date" type="text" id="from_purchase_date"
                                               style="width:80px;"></td>
                                    <td>To</td>
                                    <td><input name="to_purchase_date" type="text" id="to_purchase_date"
                                               style="width:80px;"></td>
                                   <td><input name="submit" type="button" value="Show" onClick='purchase1_report_fn();'> <input name="submit" type="button" value="Download" onClick='purchase_report_fn();'>
                                    </td>
                                </tr>
                            </form> 

                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr> -->
							<form action="outstanding_report.php" method="post" name="outstanding_report" target="_blank">
                                <tr>
                                    <td><strong>Outstanding Report </strong></td>
                                    <td style="width: 10%;text-align: right;padding-right: 10px;">From</td>
                                    <td><input class="form-control" name="from_outstanding_date" type="text" id="from_outstanding_date"
                                               style="width:80px;"></td>
                                    <td style="width: 10%;text-align: right;padding-right: 10px;">To</td>
                                    <td><input class="form-control" name="to_outstanding_date" type="text" id="to_outstanding_date"
                                               style="width:80px;"></td>
                                    <td><input class="btn btn-block btn-info"  name="submit" type="button" value="Show" onClick='outstanding_report_fn();'> 
                                    </td>
                                </tr>
                            </form>

                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>

                           <!-- <form action="sales_purchase_report.php" method="post" name="sales_purchase_report"
                                  target="_blank">
                                <tr>
                                    <td><strong>Purchase Stocks </strong></td>
                                    <td>From</td>
                                    <td><input name="from_sales_purchase_date" type="text" id="from_sales_purchase_date"
                                               style="width:80px;"></td>
                                    <td>To</td>
                                    <td><input name="to_sales_purchase_date" type="text" id="to_sales_purchase_date"
                                               style="width:80px;"></td>
                                   <!-- <td><input name="submit" type="button" value="Show"
                                               onClick='sales_purchase_report_fn();'></td> -->
                             <!--   </tr>
                            </form> -->
								<form action="customer_report.php" method="post"  name="customer_report"
                                  id="customer_report" target="myNewWinsr">
                                <tr>

                                    <td><strong>Customer report </strong></td>
                                    <td style="width: 10%;text-align: right;padding-right: 10px;">From</td>
                                    <td><input class="form-control" name="from_sales_date1" type="text" id="from_sales_date1"
                                               style="width:80px;"></td>
                                    <td style="width: 10%;text-align: right;padding-right: 10px;">To</td>
                                    <td><input class="form-control" name="to_sales_date1" type="text" id="to_sales_date1" style="width:80px;">
                                    </td>
                                    <td><input class="btn btn-block btn-info"  name="submit" type="button" value="Show" onClick='sales1_report_fn1();'>																		<!-- <input name="submit" type="button" value="Download" onClick='sales_report_fn1();'> -->
                                    </td>

                                </tr>
                                <tr>
								<td> </td>
								<td style="width: 10%;text-align: right;padding-right: 10px;">Select Customer</td>
								<td><select class="form-control" name="selectcust" id="selectcust">
								<option value="" selected>-- Select -- </option>
								<?php 
								$line = "SELECT id,customer_name from customer_details order by customer_name asc";
								$result = mysqli_query($db->connection, $line);
								 while ($row = mysqli_fetch_array($result)) {
										?>
									<option value="<?php echo $row['id']; ?>" ><?php echo $row['customer_name']; ?> </option>
								<?php

									
								} ?>
								</select>
								</td>
								</tr>
                            </form>
							<tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <form action="customer_outstanding.php" method="post"  name="customer_outstanding"
                                  id="customer_outstanding" target="myNewWinsr">
                                <tr>

                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                    </td>

                                </tr>
                                <tr>
								<td> <strong>Customer Outstanding </strong></td>
								<td><select class="form-control" name="from_out_date1" id="from_out_date1">
                                	<option value="January">January</option>
                                    <option value="February">February</option>
                                    <option value="March">March</option>
                                    <option value="April">April</option>
                                    <option value="May">May</option>
                                    <option value="June">June</option>
                                    <option value="July">July</option>
                                    <option value="August">August</option>
                                    <option value="September">September</option>
                                    <option value="October">October</option>
                                    <option value="November">November</option>
                                    <option value="December">December</option>
                                    
                                </select>
                                 </td>
								<td><select class="form-control" name="selectcustom" id="selectcustom">
								<option value="" selected>-- Select -- </option>
								<?php 
								$line1 = "SELECT customer_name from customer_details order by customer_name asc";
								$result1 = mysqli_query($db->connection, $line1);
								 while ($row1 = mysqli_fetch_array($result1)) {
										?>
									<option value="<?php echo $row1['customer_name']; ?>" ><?php echo $row1['customer_name']; ?> </option>
								<?php

									
								} ?>
								</select>
								</td>
                                <td></td>
                                <td></td>
                                <td><input class="btn btn-block btn-info"  name="submit" type="button" value="Show" onClick='sales1_report_fn12();'>
								<!--<input name="submit" type="button" value="Download" onClick='sales_report_fn12();'> --></td></td>
								</tr>
                            </form>
						<!--	<form action="" method="post"  name="new_customer_report"
                                  id="new_customer_report" target="myNewWinsr">
                                <tr>

                                    <td><strong>New Customer report </strong></td>
                                    <td>From</td>
                                    <td><input name="from_sales_date21" type="text" id="from_sales_date21"
                                               style="width:80px;"></td>
                                    <td>To</td>
                                    <td><input name="to_sales_date21" type="text" id="to_sales_date21" style="width:80px;">
                                    </td>
                                    <td><input name="submit" type="button" value="Download" onClick='new_customer_report11();'>
                                    </td>

                                </tr>
                                <tr>
								<td> </td>
								<td> </td>
								<td>&nbsp;&nbsp; 
								<select name="selectcust21" id="selectcust21">
								<option value="" selected>-- Select -- </option>
							//	<?php 
							//	$line = "SELECT customer_name from customer_details order by customer_name asc";
							//	$result = mysqli_query($db->connection, $line);
							//	 while ($row = mysqli_fetch_array($result)) {
										?>
									<option value="<?php // echo $row['customer_name']; ?>" ><?php // echo $row['customer_name']; ?> </option>
								<?php

									
								//} ?>
								</select>
								</td>
								</tr>
                            </form> -->
                        </table>


                </div>
                <!-- end content-module-main -->


            </div>
            <!-- end content-module -->


        </div>
        <!-- end full-width -->

    </div>
    <!-- end content -->
<!-- FOOTER --><div id="footer">
    <p>Any Queries email to <a href="mailto:jeetsarphare@outlook.com?subject=Print_gallery">jeetsarphare@outlook.com</a>.
    </p>

</div>
<!-- end footer -->

</body>
</html>
<?php

session_start();

require('fpdf/fpdf.php');

    include_once 'connect.php';
    $final = [''];
    $id = base64_decode($_GET["iid"]);

     $query1 = "SELECT * FROM all_inquiry WHERE id = '$id'";
    
    $final1 = mysqli_query($con,$query1);

    if (!$final1) {
                        printf("Error: %s\n", mysqli_error($con));
                        exit();
                    }


$row1 = mysqli_fetch_array($final1,MYSQLI_ASSOC);

$pdf = new FPDF();
$pdf->Addpage();
$pdf->SetFont("Arial","B",16);
$pdf->Image('logo01.png',10,6,30);
$pdf->Cell(0,10,"Receipt",0,1,"C");
$pdf->SetFont("Arial","B",12);
$pdf->Cell(65,40,"Payment Date: {$row1['drop_off_date']}",0,1);
$pdf->Cell(65,-20,"Name: {$row1['name']}",0,1);


$query2 = "SELECT * FROM assign_ride WHERE inquiry_id = '$row1[id]'";
    
    $final2 = mysqli_query($con,$query2);

    if (!$final2) {
                        printf("Error: %s\n", mysqli_error($con));
                        exit();
                    }


$row2 = mysqli_fetch_array($final2,MYSQLI_ASSOC);
$pdf->Cell(65,40,"Booking No: {$row2['booking_id']}",0,1);
$pdf->SetFont("Arial","B",12);

$pdf->Cell(95,10,"Date : ",1,0);
$pdf->Cell(95,10,"{$row1['drop_off_date']}",1,1);

$pdf->Cell(95,10,"Name : ",1,0);
$pdf->Cell(95,10,"{$row1['name']}",1,1);

$pdf->Cell(95,10,"Package : ",1,0);
$pdf->Cell(95,10,"{$row1['package']}",1,1);

$query4 = "SELECT * FROM s_package WHERE id = '$row1[sub_package]'";
    


    $final4 = mysqli_query($con,$query4);

    if (!$final4) {
                        printf("Error: %s\n", mysqli_error($con));
                        exit();
                    }


$row4 = mysqli_fetch_array($final4,MYSQLI_ASSOC);

$pdf->Cell(95,10,"Sub Package : ",1,0);
$pdf->Cell(95,10,"{$row4['sub_name']}",1,1);


$query3 = "SELECT * FROM agency WHERE a_id = '$row2[agency_id]'";
    


    $final3 = mysqli_query($con,$query3);

    if (!$final3) {
                        printf("Error: %s\n", mysqli_error($con));
                        exit();
                    }


$row3 = mysqli_fetch_array($final3,MYSQLI_ASSOC);

$pdf->Cell(95,10,"Agency : ",1,0);
$pdf->Cell(95,10,"{$row3['a_name']}",1,1);

$pdf->Cell(95,10,"From - To : ",1,0);
$pdf->Cell(95,10,"{$row1['pick_up_location']} - {$row1['drop_off_location']}",1,1);

$pdf->Cell(95,10,"Amount : ",1,0);
$pdf->Cell(95,10,"{$row1['cost']}",1,1);

$pdf->Cell(0,10,"Total Amount: {$row1['cost']} INR",1,0,"R");

$pdf->SetFont("Arial","B",10);

$pdf->Cell(0,115,"( Note : This is system generated receipt )",0,0,"R");



$to = $row1['email']; 
$from = "sales@lazytricks.com"; 
$subject = "Invoice - lazytricks"; 
$message = "<p>Please see the attachment.</p>";

// a random hash will be necessary to send mixed content
$separator = md5(time());

// carriage return type (we use a PHP end of line constant)
$eol = PHP_EOL;

// attachment name
$filename = "invoice-".$row2['booking_id'].".pdf";

// encode data (puts attachment in proper format)
$pdfdoc = $pdf->Output("", "S");
$attachment = chunk_split(base64_encode($pdfdoc));

// main header (multipart mandatory)
$headers = "From: ".$from.$eol;
$headers .= "MIME-Version: 1.0".$eol;
$headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"".$eol.$eol;

//NOTICE I changed $headers to $body!!

$body .= "Content-Transfer-Encoding: 7bit".$eol;
$body .= "This is a MIME encoded message.".$eol; //had one more .$eol

// message
$body .= "--".$separator.$eol;
$body .= "Content-Type: text/html; charset=\"iso-8859-1\"".$eol;
$body .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
$body .= $message.$eol; //had one more .$eol

// attachment
$body .= "--".$separator.$eol;
$body .= "Content-Type: application/octet-stream; name=\"".$filename."\"".$eol;
$body .= "Content-Transfer-Encoding: base64".$eol;
$body .= "Content-Disposition: attachment".$eol.$eol;
$body .= $attachment.$eol;
$body .= "--".$separator."--";

// send message NOTICE I replaced "" with $body
mail($to, $subject, $body, $headers);

?>
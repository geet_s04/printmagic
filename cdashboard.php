<?php
include_once("cinit.php");

?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Customer Dashboard</title>

    <script src="js/script.js"></script>
    <!-- Stylesheets -->

    <link rel="stylesheet" href="css/style.css">

    <!-- Optimize for mobile devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

</head>
<body>

<!-- TOP BAR -->
<?php include_once("tpl/top_bar.php"); ?>
<!-- end top-bar -->


<!-- HEADER -->
<div id="header-with-tabs">

    <div class="page-full-width cf">

       <ul id="tabs" class="fl">
            <li><a href="cdashboard.php" class="active-tab  dashboard-tab">Dashboard</a></li>
            <li><a href="view_cjobs.php" class="sales-tab">Jobs</a></li>
            <li><a href="view_cpayments.php" class="payment-tab">Payments</a></li>
        </ul>
        <!-- end tabs -->

        <!-- Change this image to your own company's logo -->
        <!-- The logo will automatically be resized to 30px height. -->
    </div>
    <!-- end full-width -->

</div>
<!-- end header -->


<!-- MAIN CONTENT -->
<div id="content">

    <div class="page-full-width cf">

        <div class="side-menu fl">

            <h3>Quick Links</h3>
            <ul>
                <li><a href="">Jobs</a></li>
                <li><a href="">Payment </a></li>
            </ul>

        </div>
        <!-- end side-menu -->

        <div class="side-content fr">

            <div class="content-module">

                <div class="content-module-heading cf">

                    <h3 class="fl">Statistics</h3>
                    <span class="fr expand-collapse-text">Click to collapse</span>
                    <span class="fr expand-collapse-text initial-expand">Click to expand</span>

                </div>
                <!-- end content-module-heading -->

                <div class="content-module-main cf">
<marquee><p style="font-size:15px;color:blue">New Office Address : SHREE GANESH OFFSET, D-22 Ravi Estate, Rustam Mill Compund, Nr Gurudwara , Dudheshwar, Ahmedabad - 380 004 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Phone No: 079-25623771 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Mobile : +91-9374063771</p></marquee>

						<?php $cid= $POSNIC['username']; 
							$name = $db->queryUniqueValue("SELECT customer_name from customer_details WHERE c_id='".$cid."' ");
						
						?>
                    <table style="width:350px; float:left;" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="250" align="left">&nbsp;</td>
                            <td width="150" align="left">&nbsp;</td>
                        </tr>
                        <?php 
						$completed = $db->queryUniqueValue("SELECT COUNT(DISTINCT transactionid) from stock_sales WHERE customer_id='$name' ");
						
						?>
                        <tr>
                            <td align="left">Total Number of Jobs </td>
                            <td align="left"><?php echo $completed; ?></td>
                        </tr>
                        
                        
                        <tr>
                            <td align="left">&nbsp;</td>
                            <td align="left">&nbsp;</td>
                        </tr>
                        <?php
						$a = $db->queryUniqueValue("SELECT SUM(payment) FROM transactions WHERE customer='$name' ");
						
						$line20 = "SELECT DISTINCT transactionid FROM stock_sales WHERE customer_id='$name' ";
						$result20 = mysqli_query($db->connection, $line20);
                        
						while ($row = mysqli_fetch_array($result20)) {
							
							$itemb = $db->queryUniqueValue("SELECT subtotal FROM stock_sales WHERE transactionid='".$row['transactionid']."' ");
							$ab = $ab + $itemb;
						}
						
						?>
						<tr>
                            <td align="left">Total Sales</td>
                            <td align="left">Rs.<?php echo $ab; ?></td>
                        </tr>
                        <tr>
                            <td align="left">Total Payment</td>
                            <td align="left">Rs.<?php echo $a; ?></td>
                        </tr>
                        <tr>
                            <td align="left">&nbsp;</td>
                            <td align="left">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="left">Total Outstanding</td>
                            <td align="left">Rs.<?php echo $ab - $a; ?></td>
                        </tr>
                        <tr>
                            <td align="left">&nbsp;</td>
                            <td align="left">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="left">&nbsp;</td>
                            <td align="left">&nbsp;</td>
                        </tr>
                   <tr>                            
						<td colspan="2"></br>
						<h1 style="font-size:20px;color:green">BANK DETAILS : </h1></br>							<p style="font-size:20px;color:red"> NAME : SHREE GANESH OFFSET </br></br>							A/C NO : 2811732910 </br></br>							BANK : KOTAK </br></br>							IFSC : KKBK0000827 </br></br>							BRANCH : SHAHIBAUG-AHMEDABAD </p>
						</td>                                                    
					</tr>                                         

					</table>

                    <table style="width:600px; margin-left:50px; float:left;" border="0" cellspacing="0"
                           cellpadding="0">
						   
						   <tr> 
							<td colspan="4"> <h1 style="font-size:40px;color:red"> PRICE LIST </h1></td>
							
						   </tr>
                         <tr>
                                    <th>No</th>
                                    <th>Particulars</th>
                                     <th>Price</th>
                                   
                                </tr>
								
                                <?php 
								
								$sql = "SELECT * FROM  list_details ORDER BY id DESC";
								$result = mysqli_query($db->connection, $sql);
								$i = 1;
                                $no = $page - 1;
                                $no = $no * $limit;
                                while ($row = mysqli_fetch_array($result)) {
                                    ?>
                                    <tr>
                                        <td> <?php echo $no + $i; ?></td>

                                        <td><?php echo $row['list_name']; ?></td>
										<td><?php echo $row['copy']; ?></td>
                                        

                                    </tr>
                                    <?php $i++;
                                } ?>
                       

                    </table>
                    <!--<ul class="temporary-button-showcase">
                        <li><a href="#" class="button round blue image-right ic-add text-upper">Add</a></li>
                        <li><a href="#" class="button round blue image-right ic-edit text-upper">Edit</a></li>
                        <li><a href="#" class="button round blue image-right ic-delete text-upper">Delete</a></li>
                        <li><a href="#" class="button round blue image-right ic-download text-upper">Download</a></li>
                        <li><a href="#" class="button round blue image-right ic-upload text-upper">Upload</a></li>
                        <li><a href="#" class="button round blue image-right ic-favorite text-upper">Favorite</a></li>
                        <li><a href="#" class="button round blue image-right ic-print text-upper">Print</a></li>
                        <li><a href="#" class="button round blue image-right ic-refresh text-upper">Refresh</a></li>
                        <li><a href="#" class="button round blue image-right ic-search text-upper">Search</a></li>
                    </ul>-->

                </div>
                <!-- end content-module-main -->


            </div>
            <!-- end content-module -->


        </div>
        <!-- end full-width -->

    </div>
</div>


<!-- FOOTER -->

<!-- FOOTER --><div id="footer">
    <p>Any Queries email to <a href="mailto:jeetsarphare@outlook.com?subject=Print_gallery">jeetsarphare@outlook.com</a>.
    </p>

</div>
<!-- end footer -->
</body>
</html>
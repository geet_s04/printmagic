<?php
include_once "init.php";
// Use session variable on this page. This function must put on the top of page.
if (!isset($_SESSION['username']) || $_SESSION['usertype'] != 'admin') { // if session variable "username" does not exist.
    header("location: index.php?msg=Please%20login%20to%20access%20admin%20area%20!"); // Re-direct to index.php
} else {

    error_reporting(0);
    if (isset($_GET['id']) && isset($_GET['table'])) {
        $id = $_GET['id'];
        $tablename = $_GET['table'];
        $return = $_GET['return'];

        if ($tablename == "stock_entries") {
            $difference = $db->queryUniqueValue("SELECT quantity FROM stock_entries WHERE id='$id'");

            $name = $db->queryUniqueObject("SELECT stock_name,material,category FROM stock_entries WHERE id='$id'");
			$nam = $name->stock_name;
			$mat = $name->material;
			$cat = $name->category;
            
            $total = $db->queryUniqueValue("SELECT quantity FROM stock_avail WHERE name='$nam' AND type='$mat' AND category='$cat' ");
            $total = $total - $difference;
            $db->execute("UPDATE stock_avail SET quantity='$total' WHERE name='$nam' AND type='$mat' AND category='$cat' ");
        }
        if ($tablename == "stock_sales") {
           $sid = $db->queryUniqueObject("SELECT customer_id,transactionid,amount,subtotal,balance FROM stock_sales WHERE id='$id'");
				$ssid = $sid->customer_id;
				$tid= $sid->transactionid;
				$amt = $sid->amount;
				$sub= $sid->subtotal;
				$oldbal= $sid->balance;
				$name = $db->queryUniqueObject("SELECT customer_name,balance FROM customer_details WHERE customer_name='$ssid'");
			   $cname = $name->customer_name;
			   $balance= $name->balance;
			   
			   $newbal = $balance - $amt;
			   
			   $chbal = $oldbal - $amt;
			   $newsub = $sub - $amt;			   			    $line12 = $db->queryUniqueObject("SELECT * FROM stock_sales WHERE id='$id' ");			   			    $descc = "The DELETE query is fired with id :" . $line12->transactionid . ",Stock Name :" . $line12->stock_name . ",Size :" .$line12->size.",Paper :" .$line12->paper.", Customer :" .$line12->customer_id.", Quantity :" .$line12->quantity.", Date :" .$line12->date.", Username :" .$line12->username.", Entry type: used, Bill No :" .$line12->billnumber.", Amount :".$line12->amount ;										$db->query("INSERT INTO `history`(`query`,`table_name`,`description`) VALUES ('DELETE','stock_sales','$descc')");			   			   
			   $db->execute("UPDATE customer_details SET balance='$newbal' WHERE customer_name='$cname' ");
			   
			   $db->execute("UPDATE stock_sales SET subtotal='$newsub',grand_total='$newsub',balance='$chbal' WHERE transactionid='$tid'  ");
           }if ($tablename == "purchase_detail") {
           $sid = $db->queryUniqueObject("SELECT * FROM purchase_detail WHERE id='$id'");
				$tid= $sid->purchase_id;
				$amt = $sid->total;
				$sub= $sid->subtotal;
				$oldbal= $sid->balance;
				
			   
			   $chbal = $oldbal - $amt;
			   $newsub = $sub - $amt;
			   $db->execute("UPDATE purchase_detail SET subtotal='$newsub',balance='$chbal' WHERE purchase_id='$tid'  ");
           }
		   if ($tablename == "expense_detail") {
           $sid = $db->queryUniqueObject("SELECT * FROM expense_detail WHERE id='$id'");
				$tid= $sid->exp_id;
				$amt = $sid->total;
				$sub= $sid->subtotal;
				
			   $newsub = $sub - $amt;
			   $db->execute("UPDATE expense_detail SET subtotal='$newsub' WHERE exp_id='$tid'  ");
           }		   if ($tablename == "transactions") {           $line14 = $db->queryUniqueObject("SELECT * FROM stock_sales WHERE id='$id' ");			   			    $descc = "The DELETE query is fired with id :" . $line14->rid . ",Mode :" . $line14->mode . ",Payment :" .$line14->payment.",Type :" .$line14->type.", Customer :" .$line14->customer.", Date :" .$line14->date.", Username :" .$line14->username.", Receipt Id :" .$line14->receiptid ;										$db->query("INSERT INTO `history`(`query`,`table_name`,`description`) VALUES ('DELETE','transactions','$descc')");           }
        $id = $_GET['id'];

        $db->execute("DELETE FROM $tablename WHERE id='$id'");

        header("location: $return?msg=Record Deleted Successfully!&id=$id");
    }
    if (isset($_POST['table']) && isset($_POST['checklist'])) {
        $tablename = $_POST['table'];
    	$return = $_POST['return'];
        $i = 0;
    foreach ($_POST['checklist'] as $singleVar) {
			if ($tablename == "stock_sales") {
                $id = $singleVar;
                $sid = $db->queryUniqueObject("SELECT customer_id,transactionid,amount,subtotal,balance FROM stock_sales WHERE id='$id'");
				$ssid = $sid->customer_id;
				$tid= $sid->transactionid;
				$amt = $sid->amount;
				$sub= $sid->subtotal;
				$oldbal= $sid->balance;
				$name = $db->queryUniqueObject("SELECT customer_name,balance FROM customer_details WHERE customer_name='$ssid'");
			   $cname = $name->customer_name;
			   $balance= $name->balance;
			   
			   $newbal = $balance - $amt;
			   
			   $chbal = $oldbal - $amt;
			   $newsub = $sub - $amt;			   			    $line13 = $db->queryUniqueObject("SELECT * FROM stock_sales WHERE id='$id' ");			   			    $descc = "The DELETE query is fired with id :" . $line13->transactionid . ",Stock Name :" . $line13->stock_name . ",Size :" .$line13->size.",Paper :" .$line13->paper.", Customer :" .$line13->customer_id.", Quantity :" .$line13->quantity.", Date :" .$line13->date.", Username :" .$line13->username.", Entry type: used, Bill No :" .$line13->billnumber.", Amount :".$line13->amount ;										$db->query("INSERT INTO `history`(`query`,`table_name`,`description`) VALUES ('DELETE','stock_sales','$descc')");
			   $db->execute("UPDATE customer_details SET balance='$newbal' WHERE customer_name='$cname' ");
			   
			   $db->execute("UPDATE stock_sales SET subtotal='$newsub',grand_total='$newsub',balance='$chbal' WHERE transactionid='$tid'  ");
            }
		if ($tablename == "stock_entries") {
			 $id = $singleVar;
            $difference = $db->queryUniqueValue("SELECT quantity FROM stock_entries WHERE id='$id'");

            $name = $db->queryUniqueObject("SELECT stock_name,material,category FROM stock_entries WHERE id='$id'");
			$nam = $name->stock_name;
			$mat = $name->material;
			$cat = $name->category;
            
            $total = $db->queryUniqueValue("SELECT quantity FROM stock_avail WHERE name='$nam' AND type='$mat' AND category='$cat' ");
            $total = $total - $difference;
            $db->execute("UPDATE stock_avail SET quantity='$total' WHERE name='$nam' AND type='$mat' AND category='$cat' ");
        }
		if ($tablename == "purchase_detail") {
			 $id = $singleVar;
           $sid = $db->queryUniqueObject("SELECT * FROM purchase_detail WHERE id='$id'");
				$tid= $sid->purchase_id;
				$amt = $sid->total;
				$sub= $sid->subtotal;
				$oldbal= $sid->balance;
				
			   
			   $chbal = $oldbal - $amt;
			   $newsub = $sub - $amt;
			   $db->execute("UPDATE purchase_detail SET subtotal='$newsub',balance='$chbal' WHERE purchase_id='$tid'  ");
           }
		   if ($tablename == "expense_detail") {
			 $id = $singleVar;
             $sid = $db->queryUniqueObject("SELECT * FROM expense_detail WHERE id='$id'");
				$tid= $sid->exp_id;
				$amt = $sid->total;
				$sub= $sid->subtotal;
				
				$newsub = $sub - $amt;
			   $db->execute("UPDATE expense_detail SET subtotal='$newsub' WHERE exp_id='$tid'  ");
           }
	      mysqli_query($db->connection, "DELETE FROM $tablename WHERE id='$singleVar'") or die(mysqli_error());

            $i++;
    }
        header("location: $return?msg=Record Deleted Successfully!");
    }
    if (isset($_POST['return'])) {
        header("location: $return");
    }
}
?>
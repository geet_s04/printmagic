<?php
include_once("init.php");
?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>View Sales</title>

    <!-- Stylesheets -->
    <!---->
    <link rel="stylesheet" href="css/style.css">

    <!-- Optimize for mobile devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- jQuery & JS files -->
    <?php //include_once("tpl/common_js.php"); ?>
    <!-- <script src="js/script.js"></script> -->


    

</head>
<body>

<!-- TOP BAR -->
<?php include_once("tpl/top_bar.php"); ?>
<!-- end top-bar -->


<!-- HEADER -->
<?php include_once("tpl/header.php"); ?>
<!-- end header -->


<!-- MAIN CONTENT -->
<div id="content">

    <div class="page-full-width cf">

        <div class="side-menu fl">

            <h3>Sales</h3>
            <ul>
                <li><a href="add_sales.php?page=sales">Add Sales</a></li>
                <li><a href="view_sales.php?page=sales">View Sales</a></li>								<li style="background-color:#f99d9d;color:white;padding: 20px;">IN PROGRESS</li>				<li style="background-color:#62ca62;color:white;padding: 20px;">COMPLETED</li>								<li style="background-color:#ffff6f;color:black;padding: 20px;">DELIVERED</li>

            </ul>

        </div>
        <!-- end side-menu -->

        <div class="side-content fr">

            <div class="content-module">

                <div class="content-module-heading cf">

                    <h3 class="fl">Sales</h3>
                    <span class="fr expand-collapse-text">Click to collapse</span>
                    <span class="fr expand-collapse-text initial-expand">Click to expand</span>

                </div>
                <!-- end content-module-heading -->

                <div class="content-module-main cf">
                <table id="example" class="table table-striped table-bordered nowrap" style="width:100%">
        <thead>
            <tr>
            <th>No</th>
            <th>Job No</th>
            <th>Date</th>
            <th>Customer</th>
            <th>Product</th>
            <th>Size</th>
            <th>Paper</th>
            <th>Amount</th>
            <th>Edit /Delete</th>
            </tr>
        </thead>
        <tbody>
        <?php
                                 $sql = "SELECT * FROM stock_sales ORDER BY date desc  ";
                                 $result = mysqli_query($db->connection, $sql);
                                $i = 1;
                                $no = $page - 1;
                                $no = $no * $limit;
                                while ($row = mysqli_fetch_array($result)) {																						
                                    ?>
                                    <tr>
                                        <td width="20px"> <?php echo $no + $i; ?></td>
                                        <!-- <td ><a href="add_sales_print.php?sid=<?php // echo $row['transactionid']; ?>"> <?php //echo $row['transactionid']; ?></a></td> -->
										<td ><?php echo $row['job_no']; ?></td>
                                        <td width="150px"> <?php $selected_date = strtotime($row['date']); echo date('d/m/Y',$selected_date); ?></td>
                                        <td width="150px"> <?php echo $row['customer_id']; ?></td>
                                        <td><?php echo $row['stock_name']; ?></td>
										<td width="150px"><?php echo $row['size']; ?></td>
										<td><?php echo $row['paper']; ?></td>
                                       
                                        <td><?php echo $row['amount']; ?></td>

                                        <td>
                                            <a href="update_sales.php?sid=<?php echo $row['id']; ?>&table=stock_sales&return=view_sales.php"
                                               class="table-actions-button ic-table-edit">
                                            </a>
                                            <a onclick="return confirmSubmit()"
                                               href="delete.php?id=<?php echo $row['id']; ?>&table=stock_sales&return=view_sales.php"
                                               class="table-actions-button ic-table-delete"></a>
                                        </td>

                                    </tr>
                                    <?php $i++;
                                } ?>
                        
        </tbody>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Job No</th>
                <th>Date</th>
                <th>Customer</th>
                <th>Product</th>
                <th>Size</th>
                <th>Paper</th>
                <th>Amount</th>
                <th>Edit /Delete</th>
            </tr>
        </tfoot>
    </table>

                </div>
            </div>
        </div>
    </div>
    <!-- FOOTER -->
    <div id="footer">
    <p>Any Queries email to <a href="mailto:jeetsarphare@outlook.com?subject=Print_gallery">jeetsarphare@outlook.com</a>.
    </p>
    </div>

</div>
<!-- end footer -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.7/js/responsive.bootstrap.min.js"></script>
<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        responsive: true
    } );
} );
</script>
<script LANGUAGE="JavaScript">

        function confirmSubmit() {
            var agree = confirm("Are you sure you wish to Delete this Entry?");
            if (agree)
                return true;
            else
                return false;
        }

        function confirmDeleteSubmit() {
            var flag = 0;
            var field = document.forms.deletefiles;
            for (i = 0; i < field.length; i++) {
                if (field[i].checked == true) {
                    flag = flag + 1;

                }

            }
            if (flag < 1) {
                alert("You must check one and only one checkbox!");
                return false;
            } else {
                var agree = confirm("Are you sure you wish to Delete Selected Record?");
                if (agree)

                    document.deletefiles.submit();
                else
                    return false;

            }
        }
        function confirmLimitSubmit() {
            if (document.getElementById('search_limit').value != "") {

                document.limit_go.submit();

            } else {
                return false;
            }
        }


        function checkAll() {

            var field = document.forms.deletefiles;
            for (i = 0; i < field.length; i++)
                field[i].checked = true;
        }

        function uncheckAll() {
            var field = document.forms.deletefiles;
            for (i = 0; i < field.length; i++)
                field[i].checked = false;
        }
      
    </script>
    <script>


        /*$.validator.setDefaults({
         submitHandler: function() { alert("submitted!"); }
         });*/
        $(document).ready(function () {

            // validate signup form on keyup and submit
            $("#form1").validate({
                rules: {
                    name: {
                        required: true,
                        minlength: 3,
                        maxlength: 200
                    },
                    address: {
                        minlength: 3,
                        maxlength: 500
                    },
                    contact1: {
                        minlength: 3,
                        maxlength: 20
                    },
                    contact2: {
                        minlength: 3,
                        maxlength: 20
                    }
                },
                messages: {
                    name: {
                        required: "Please enter a supplier Name",
                        minlength: "supplier must consist of at least 3 characters"
                    },
                    address: {
                        minlength: "supplier Address must be at least 3 characters long",
                        maxlength: "supplier Address must be at least 3 characters long"
                    }
                }
            });

        });

    </script>
</body>
</html>
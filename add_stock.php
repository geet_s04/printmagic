<?php
include_once("init.php");

?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Add Stock</title>

    <!-- Stylesheets -->

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="js/date_pic/date_input.css">
    <link rel="stylesheet" href="lib/auto/css/jquery.autocomplete.css">

    <!-- Optimize for mobile devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- jQuery & JS files -->
    <?php include_once("tpl/common_js.php"); ?>
    <script src="js/script.js"></script>
    <script src="js/date_pic/jquery.date_input.js"></script>
    <script src="lib/auto/js/jquery.autocomplete.js "></script>
	<script type="text/javascript">
		$("#quty").blur(function () {
                if (document.getElementById('item').value == "") {
                    document.getElementById('item').focus();
				}
		});
		</script>
		

    <script>
        /*$.validator.setDefaults({
         submitHandler: function() { alert("submitted!"); }
         });*/
        $(document).ready(function () {
            $("#supplier").autocomplete("supplier1.php", {
                width: 160,
                autoFill: true,
                selectFirst: true
            });
            $("#category").autocomplete("category.php", {
                width: 160,
                autoFill: true,
                selectFirst: true
            });
            // validate signup form on keyup and submit
            $("#form1").validate({
                rules: {
                    name: {
                        required: true,
                        minlength: 3,
                        maxlength: 200
                    },
                    width: {
                        required: true
                    },
                    height: {
                        required: true
                    },
                    stockid: {
                        required: true,
                        minlength: 3,
                        maxlength: 200
                    }
                    
                },
                messages: {
                    name: {
                        required: "Please Enter Stock Name",
                        minlength: "Name must consist of at least 3 characters"
                    },
                    stockid: {
                        required: "Please Enter Stock ID",
                        minlength: "Category Name must consist of at least 3 characters"
                    }
                }
            });

        });
        function numbersonly(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
            if (unicode != 8 && unicode != 46 && unicode != 37 && unicode != 38 && unicode != 39 && unicode != 40 && unicode != 9) { //if the key isn't the backspace key (which we should allow)
                if (unicode < 48 || unicode > 57)
                    return false
            }
        }
		
    </script>


</head>

<body>

<!-- TOP BAR -->
<?php include_once("tpl/top_bar.php"); ?>
<!-- end top-bar -->


<!-- HEADER -->
<div id="header-with-tabs">

    <div class="page-full-width cf">

        <ul id="tabs" class="fl">
            <li><a href="dashboard.php" class="dashboard-tab">Dashboard</a></li>
            <li><a href="view_sales.php" class="sales-tab">Sales</a></li>
            <li><a href="view_customers.php" class=" customers-tab">Customers</a></li>
            <li><a href="view_purchase.php" class="purchase-tab">Purchase</a></li>
            <li><a href="view_supplier.php" class=" supplier-tab">Supplier</a></li>
            <li><a href="view_stock_availability.php" class="active-tab stock-tab">Stocks / Products</a></li>
            <li><a href="view_payments.php" class="payment-tab">Payments / Outstandings</a></li>
            <li><a href="" class="report-tab">Reports</a></li>
        </ul>
        <!-- end tabs -->

        <!-- Change this image to your own company's logo -->
        <!-- The logo will automatically be resized to 30px height. -->
        <a href="#" id="company-branding-small" class="fr"><img src="<?php if (isset($_SESSION['logo'])) {
                echo "upload/" . $_SESSION['logo'];
            } else {
                echo "upload/posnic.png";
            } ?>" alt="Point of Sale"/></a>

    </div>
    <!-- end full-width -->

</div>
<!-- end header -->


<!-- MAIN CONTENT -->
<div id="content">

    <div class="page-full-width cf">

        <div class="side-menu fl">

            <h3>Stock Management</h3>
           <ul>
            	<li><a href="increase_stock.php">Add/Increase Stock</a></li>
                <li><a href="add_stock.php">Add Stock Detail</a></li>
                <li><a href="add_category.php">Add Stock Category</a></li>
                <li><a href="view_category.php">view Stock Category</a></li>
                <li><a href="view_stock_availability.php">view Stock Available</a></li>
                <li><a href="view_stock.php">Add View Purchase Detail</a></li>
                
            </ul>

        </div>
        <!-- end side-menu -->

        <div class="side-content fr">

            <div class="content-module">

                <div class="content-module-heading cf">

                    <h3 class="fl">Add Stock </h3>
                    <span class="fr expand-collapse-text">Click to collapse</span>
                    <span class="fr expand-collapse-text initial-expand">Click to expand</span>

                </div>
                <!-- end content-module-heading -->

                <div class="content-module-main cf">


                    <?php
                    //Gump is libarary for Validatoin

                    if (isset($_POST['name'])) {
                        $_POST = $gump->sanitize($_POST);
                        $gump->validation_rules(array(
                            'name' => 'required|max_len,100|min_len,3',
                            'width' => 'required',
                            'height' => 'required',
                            'stockid' => 'required|max_len,200',
                            'category' => 'max_len,200'

                        ));

                        $gump->filter_rules(array(
                            'name' => 'trim|sanitize_string|mysqli_escape',
                            'width' => 'trim|sanitize_string|mysqli_escape',
                            'height' => 'trim|sanitize_string|mysqli_escape',
                            'stockid' => 'trim|sanitize_string|mysqli_escape',
                            'category' => 'trim|sanitize_string|mysqli_escape'

                        ));

                        $validated_data = $gump->run($_POST);
                        $name = "";
                        $stockid = "";
                        $category = "";
						$type="";
                        $width = $height = "";

                        if ($validated_data === false) {
                            echo $gump->get_readable_errors(true);
                        } else {


                            $name = mysqli_real_escape_string($db->connection, $_POST['name']);
                            $width = mysqli_real_escape_string($db->connection, $_POST['width']);
                            $height = mysqli_real_escape_string($db->connection, $_POST['height']);
                            $stockid = mysqli_real_escape_string($db->connection, $_POST['stockid']);
                          //  $sell = mysqli_real_escape_string($db->connection, $_POST['sell']);
                           // $cost = mysqli_real_escape_string($db->connection, $_POST['cost']);
                           // $supplier = mysqli_real_escape_string($db->connection, $_POST['supplier']);
                            $category = mysqli_real_escape_string($db->connection, $_POST['category']);
							$quty = 0;
                            $type = mysqli_real_escape_string($db->connection, $_POST['type']);
							$max1 = $db->maxOfAll("id", "stock_details");
                                $max2 = $max1 + 1;
								
                            $count = $db->countOf("stock_details", "stock_id ='$stockid'");
                            if ($count == 1) {
                                echo "<font color=red> Dublicate Entry. Please Verify</font>";
                            } else {

                                if ($db->query("insert into stock_details(stock_id,stock_name,stock_width,stock_height,stock_quatity,stock_type,category) values('$stockid','$name','$width','$height','$quty','$type','$category')")) {
                                    echo "<br><font color=green size=+1 > [ $name ] Stock Details Added !</font>";
                                     $idd = $db->queryUniqueValue("SELECT id FROM stock_details WHERE stock_id='$stockid'");
									$db->query("insert into stock_avail(stock_id,name,quantity,type,category) values('$idd','$name','$quty','$type','$category')");
                                } else
                                    echo "<br><font color=red size=+1 >Problem in Adding !</font>";

                            }


                        }

                    }
                    ?>

                    <form name="form1" method="post" id="form1" action="">


                        <table class="form" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <?php
                                $max = $db->maxOfAll("id", "stock_details");
                                $max = $max + 1;
                                $autoid = "SD" . $max . "";
                                ?>
                                <td><span class="man">*</span>Stock ID:</td>
                                <td><input name="stockid" type="text" id="stockid" readonly maxlength="200"
                                           class="round default-width-input"
                                           value="<?php echo isset($autoid) ? $autoid : ''; ?>"/></td>

                                <td><span class="man">*</span>Size:</td>
                                <td><input name="name" placeholder="ENTER SIZE TITLE" type="text" id="name"
                                           maxlength="200" class="round default-width-input"
                                           value=""/></td>

                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                   <input name="width" placeholder="ENTER WIDTH" type="text" id="width" maxlength="200" class="" value="" size="10"/>
                                   <input name="height" placeholder="ENTER HEIGHT" type="text" id="height" maxlength="200" class="" value="" size="11"/> 
                                </td>
                            </tr>
                            <tr>
                                
                                <td>Category:</td>
                                <td><input name="category" placeholder="ENTER CATEGORY NAME" type="text" id="category"
                                           maxlength="200" class="round default-width-input"
                                           value=""/></td>
								<!--<td>Stock:</td>
								<td><input name="quty" type="text" id="quty" maxlength="200"
                                           class="round default-width-input my_with"
                                           onKeyPress="quantity_chnage(event);return numbersonly(event);"
                                           onkeyup="total_amount();unique_check()"/></td>
								<td><input type="button" onclick="add_values()" onkeyup=" balance_amount();"
                                           id="add_new_code"
                                           style="margin-left:30px; width:30px;height:30px;border:none;background:url(images/add_new.png)"
                                           class="round"></td>
									-->	   
								<td>Type:</td>
                                <td><input name="type" placeholder="GSM" type="text" id="type"
                                           maxlength="50" class="round default-width-input"
                                           value=""/></td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>


                            <tr>
                                <td>&nbsp;
                                    
                                </td>
                                <td>
                                    <input class="button round blue image-right ic-add text-upper" type="submit"
                                           name="Submit" value="Add">
                                    (Control + S)

                                <td align="right"><input class="button round red   text-upper" type="reset" name="Reset"
                                                         value="Reset"></td>
                            </tr>
                        </table>
                    </form>


                </div>
                <!-- end content-module-main -->


            </div>
            <!-- end content-module -->


        </div>
        <!-- end full-width -->

    </div>
    <!-- end content -->


    
<!-- FOOTER --><div id="footer">
    <p>Any Queries email to <a href="mailto:jeetsarphare@outlook.com?subject=Print_gallery">jeetsarphare@outlook.com</a>.
    </p>

</div>
<!-- end footer -->

</body>
</html>
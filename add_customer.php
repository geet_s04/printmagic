<?php
include_once("init.php");

?>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Add Customer</title>

    <!-- Stylesheets -->

    <link rel="stylesheet" href="css/style.css">

    <!-- Optimize for mobile devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style type="text/css">
        body {
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
            background-color: #FFFFFF;
        }

        * {
            padding: 0px;
            margin: 0px;
        }

        #vertmenu {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 100%;
            width: 160px;
            padding: 0px;
            margin: 0px;
        }

        #vertmenu h1 {
            display: block;
            background-color: #FF9900;
            font-size: 90%;
            padding: 3px 0 5px 3px;
            border: 1px solid #000000;
            color: #333333;
            margin: 0px;
            width: 159px;
        }

        #vertmenu ul {
            list-style: none;
            margin: 0px;
            padding: 0px;
            border: none;
        }

        #vertmenu ul li {
            margin: 0px;
            padding: 0px;
        }

        #vertmenu ul li a {
            font-size: 80%;
            display: block;
            border-bottom: 1px dashed #C39C4E;
            padding: 5px 0px 2px 4px;
            text-decoration: none;
            color: #666666;
            width: 160px;
        }

        #vertmenu ul li a:hover,
        #vertmenu ul li a:focus {
            color: #000000;
            background-color: #eeeeee;
        }

        .style1 {
            color: #000000
        }

        div.pagination {

            padding: 3px;

            margin: 3px;

        }

        div.pagination a {

            padding: 2px 5px 2px 5px;

            margin: 2px;

            border: 1px solid #AAAADD;

            text-decoration: none;
            /* no underline */

            color: #000099;

        }

        div.pagination a:hover,
        div.pagination a:active {

            border: 1px solid #000099;

            color: #000;

        }

        div.pagination span.current {

            padding: 2px 5px 2px 5px;

            margin: 2px;

            border: 1px solid #000099;

            font-weight: bold;

            background-color: #000099;

            color: #FFF;

        }

        div.pagination span.disabled {

            padding: 2px 5px 2px 5px;

            margin: 2px;

            border: 1px solid #EEE;

            color: #DDD;

        }
    </style>
    <!-- jQuery & JS files -->
    <?php include_once("tpl/common_js.php"); ?>
    <script src="js/script.js"></script>
    <script>
        /*$.validator.setDefaults({
         submitHandler: function() { alert("submitted!"); }
         });*/
        $(document).ready(function() {
            $('#job_work').hide();
            // $("#is_jobwork").on('change', function (){
            //     $('#job_work').show();
            // });
            $('#is_jobwork').click(function() {
                $("#job_work").toggle(this.checked);
            });
            // validate signup form on keyup and submit
            $("#form1").validate({
                rules: {
                    name: {
                        required: true,
                        minlength: 3,
                        maxlength: 200
                    },
                    address: {
                        minlength: 3,
                        maxlength: 500
                    },
                    contact1: {
                        minlength: 3,
                        maxlength: 20
                    },
                    contact2: {
                        minlength: 3,
                        maxlength: 100
                    }
                },
                messages: {
                    name: {
                        required: "Please enter a Customer Name",
                        minlength: "Customer must consist of at least 3 characters"
                    },
                    address: {
                        minlength: "Customer Address must be at least 3 characters long",
                        maxlength: "Customer Address must be at least 3 characters long"
                    }
                }
            });

        });
    </script>

</head>

<body>

    <!-- TOP BAR -->
    <?php include_once("tpl/top_bar.php"); ?>
    <!-- end top-bar -->


    <!-- HEADER -->
    <?php include_once("tpl/header.php"); ?>
    <!-- end header -->


    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="page-full-width cf">

            <div class="side-menu fl">

                <h3>Customers Management</h3>
                <ul>
                    <li><a href="add_customer.php">Add Customer</a></li>
                    <li><a href="view_customers.php">View Customers</a></li>
                </ul>

            </div>
            <!-- end side-menu -->

            <div class="side-content fr">

                <div class="content-module">

                    <div class="content-module-heading cf">

                        <h3 class="fl">Add Customer</h3>
                        <span class="fr expand-collapse-text">Click to collapse</span>
                        <span class="fr expand-collapse-text initial-expand">Click to expand</span>

                    </div>
                    <!-- end content-module-heading -->

                    <div class="content-module-main cf">


                        <?php
                        //Gump is libarary for Validatoin

                        if (isset($_POST['name'])) {
                            $_POST = $gump->sanitize($_POST);
                            $gump->validation_rules(array(
                                'name' => 'required|max_len,100|min_len,3',
                                'address' => 'max_len,200',
                                'contact1' => 'alpha_numeric|max_len,20',
                                'contact2' => 'max_len,100'
                            ));

                            $gump->filter_rules(array(
                                'name' => 'trim|sanitize_string|mysqli_escape',
                                'address' => 'trim|sanitize_string|mysqli_escape',
                                'contact1' => 'trim|sanitize_string|mysqli_escape',
                                'contact2' => 'trim|sanitize_string|mysqli_escape'
                            ));

                            $validated_data = $gump->run($_POST);
                            $name = "";
                            $address = "";
                            $contact1 = "";
                            $contact2 = "";
                            
                            if ($validated_data === false) {
                                echo $gump->get_readable_errors(true);
                            } else {

                                $userid = mysqli_real_escape_string($db->connection, $_POST['uid']);
                                $pwd = mysqli_real_escape_string($db->connection, $_POST['pwd']);
                                $name = mysqli_real_escape_string($db->connection, $_POST['name']);
                                $address = mysqli_real_escape_string($db->connection, $_POST['address']);
                                $contact1 = mysqli_real_escape_string($db->connection, $_POST['contact1']);
                                $contact2 = mysqli_real_escape_string($db->connection, $_POST['contact2']);
                                //$is_jobwork = mysqli_real_escape_string($db->connection, $_POST['contact2']);
                                $printing = mysqli_real_escape_string($db->connection, $_POST['printing']);
                                $lamination = mysqli_real_escape_string($db->connection, $_POST['lamination']);
                                $drip_Off = mysqli_real_escape_string($db->connection, $_POST['drip-off']);
                                $punching = mysqli_real_escape_string($db->connection, $_POST['punching']);
                                $pasting = mysqli_real_escape_string($db->connection, $_POST['pasting']);
                                $foils = mysqli_real_escape_string($db->connection, $_POST['foils']);
                                $embose = mysqli_real_escape_string($db->connection, $_POST['embose']);
                                $parscel = mysqli_real_escape_string($db->connection, $_POST['parscel']);
                                $other1 = mysqli_real_escape_string($db->connection, $_POST['other1']);
                                $other2 = mysqli_real_escape_string($db->connection, $_POST['other2']);

                                if($pwd == NULL){
                                    $pwd = $userid;
                                }
                                if(isset($_POST['is_jobwork'])){
                                    $is_jobwork = "yes";
                                }else{
                                    $is_jobwork = "no";
                                }
                                //exit;
                                $count = $db->countOf("customer_details", "customer_name='$name'");
                                $count1 = $db->countOf("customer_details", "c_id='$userid'");
                                if ($count == 1 || $count1 == 1) {
                                    if ($count == 1) {
                                        echo "<div class='error-box round'>Dublicate Customer Name Entry. Please use different Name</div>";
                                    } else {
                                        echo "<div class='error-box round'>Dublicate User id entered. Please use different id</div>";
                                    }
                                } else {

                                    if ($db->query("insert into customer_details values(NULL,'$userid','$pwd','$name','$address','$contact1','$contact2',0)")) {
                                        //$db->lastInsertedId("customer_details");
                                        $customer_id = $db->lastInsertedId();
                                            $db->query("insert into customer_pricing values(NULL,'$customer_id','$printing','$lamination',
                                            '$drip_Off','$punching','$pasting','$foils','$embose','$parscel','$is_jobwork','$other1','$other2',NULL)");
                                        
                                        $db->query("insert into stock_user values(NULL,'$userid','$pwd','user','$userid')");
                                        echo "<div class='confirmation-box round'>[ $name ] Customer Details Added !</div>";
                                    } else {
                                        echo "<div class='error-box round'>Problem in Adding !</div>";
                                    }
                                }
                            }
                        }

                        ?>
                        <?php
                        $max = $db->maxOfAll("id", "customer_details");
                        $max = $max + 1;
                        $autoid = "user" . $max . "";
                        ?>
                        <form name="form1" method="post" id="form1" action="">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box box-danger">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Add Customer Details</h3>
                                            </div>
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-xs-4">
                                                        <div class="form-group">
                                                            <label>Customer Name</label>
                                                            <input type="text" name="name" placeholder="ENTER YOUR FULL NAME" id="name" class="form-control input-lg" placeholder="Printing">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <div class="form-group">
                                                            <label>Contact NUmber</label>
                                                            <input name="contact1" placeholder="ENTER YOUR CONTACT NUMBER" type="text" id="buyingrate" class="form-control input-lg" placeholder="Lamination">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <div class="form-group">
                                                            <label>Address</label>
                                                            <textarea rows="3" name="address" placeholder="ENTER YOUR ADDRESS" class="form-control input-lg" placeholder="Drip-Off"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-4">
                                                        <div class="form-group">
                                                            <label>E-mail</label>
                                                            <input name="contact2" placeholder="ENTER E-MAIL" type="text" class="form-control input-lg" placeholder="Pasting">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <div class="form-group">
                                                            <label>User ID</label>
                                                            <input type="text" name="uid" placeholder=""  value="<?php echo $autoid ?>" class="form-control input-lg" placeholder="Foils">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <div class="form-group">
                                                            <label>Password</label>
                                                            <input name="pwd" placeholder="ENTER PASSWORD" type="text" class="form-control input-lg" placeholder="Embose">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-10">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" id="is_jobwork" name="is_jobwork" value="no"> Is Job Work ?
                                                        </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                    </div>
                                </div>
                         </div>
                            <!-- <table class="form" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><span class="man">*</span>Name:</td>
                                    <td><input name="name" placeholder="ENTER YOUR FULL NAME" type="text" id="name" maxlength="200" class="round default-width-input" value="" /></td>
                                    <td>Contact</td>
                                    <td><input name="contact1" placeholder="ENTER YOUR CONTACT NUMBER" type="text" id="buyingrate" maxlength="20" class="round default-width-input" value="" /></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Address</td>
                                    <td><textarea name="address" placeholder="ENTER YOUR ADDRESS" cols="15" class="round full-width-textarea"></textarea>
                                    </td>
                                    <td>E mail</td>
                                    <td><input name="contact2" placeholder="ENTER E-MAIL" type="text" id="sellingrate" maxlength="100" class="round default-width-input" value="" /></td>

                                </tr>
                                <tr>
                                    <td>User Id</td>
                                    <td><input type="text" name="uid" placeholder="" style="width:130px " value="<?php //echo $autoid ?>" class="round default-width-input" />
                                    </td>
                                    <td>Password</td>
                                    <td><input name="pwd" placeholder="ENTER PASSWORD" type="text" id="" maxlength="100" class="round default-width-input" value="" /></td>

                                </tr>

                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table> -->
                        


                    </div>
                    <!-- end content-module-main -->
                    <div class="container">
                        <div class="row" id="job_work">
                            <div class="col-md-12">
                                <div class="box box-danger">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Job Work</h3>
                                    </div>
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <div class="form-group">
                                                    <label>Printing</label>
                                                    <input type="text" class="form-control input-lg" name="printing" placeholder="Printing">
                                                </div>
                                            </div>
                                            <div class="col-xs-3">
                                                <div class="form-group">
                                                    <label>Lamination</label>
                                                    <input type="text" class="form-control input-lg" name="lamination" placeholder="Lamination">
                                                </div>
                                            </div>
                                            <div class="col-xs-3">
                                                <div class="form-group">
                                                    <label>Drip-Off</label>
                                                    <input type="text" class="form-control input-lg" name="drip-off" placeholder="Drip-Off">
                                                </div>
                                            </div>
                                            <div class="col-xs-3">
                                                <div class="form-group">
                                                    <label>Punching</label>
                                                    <input type="text" class="form-control input-lg" name="punching" placeholder="Punching">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <div class="form-group">
                                                    <label>Pasting</label>
                                                    <input type="text" class="form-control input-lg" name="pasting" placeholder="Pasting">
                                                </div>
                                            </div>
                                            <div class="col-xs-3">
                                                <div class="form-group">
                                                    <label>Foils</label>
                                                    <input type="text" class="form-control input-lg" name="foils" placeholder="Foils">
                                                </div>
                                            </div>
                                            <div class="col-xs-3">
                                                <div class="form-group">
                                                    <label>Embose</label>
                                                    <input type="text" class="form-control input-lg" name="embose" placeholder="Embose">
                                                </div>
                                            </div>
                                            <div class="col-xs-3">
                                                <div class="form-group">
                                                    <label>Parscel</label>
                                                    <input type="text" class="form-control input-lg" name="parscel" placeholder="Parscel">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <div class="form-group">
                                                    <label>Other 1</label>
                                                    <input type="text" class="form-control input-lg" name="other1" placeholder="">
                                                </div>
                                            </div>
                                            <div class="col-xs-3">
                                                <div class="form-group">
                                                    <label>Other 2</label>
                                                    <input type="text" class="form-control input-lg" name="other2" placeholder="">
                                                </div>
                                            </div>
                                            <div class="col-xs-3">
                                                
                                            </div>
                                            <div class="col-xs-3">
                                               
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="display: inline-flex;">
                                <div class="submit-col">
                                    <input class="button round blue image-right ic-add text-upper" type="submit" name="Submit" value="Add">
                                        (Control + S)
                                </div>
                                <div class="reset-col">
                                    <input class="button round red text-upper" type="reset" name="Reset" value="Reset">
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <!-- end content-module -->


            </div>
            <!-- end full-width -->

        </div>
        <!-- end content -->

        <!-- FOOTER -->
        <div id="footer">
            <p>Any Queries email to <a href="mailto:jeetsarphare@outlook.com?subject=Print_gallery">jeetsarphare@outlook.com</a>.
            </p>

        </div>
        <!-- end footer -->


</body>

</html>
<?php
include_once("init.php");

?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Update Sales</title>

    <!-- Stylesheets -->

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="js/date_pic/date_input.css">
    <link rel="stylesheet" href="lib/auto/css/jquery.autocomplete.css">

    <!-- Optimize for mobile devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- jQuery & JS files -->
    <?php include_once("tpl/common_js.php"); ?>
    <script src="js/date_pic/jquery.date_input.js"></script>
    <script src="lib/auto/js/jquery.autocomplete.js "></script>
    <script src="js/script.js"></script>
    <script>
        /*$.validator.setDefaults({
         submitHandler: function() { alert("submitted!"); }
         });*/
        $(document).ready(function () {

            // validate signup form on keyup and submit
            $("#form1").validate({
                rules: {
                    bill_no: {
                        required: false,
                        minlength: 3

                    },
                    stockid: {
                        required: true
                    },
                    grand_total: {
                        required: true
                    },
                    supplier: {
                        required: true,
                    }
                },
                messages: {
                    supplier: {
                        required: "Please Enter Supplier"
                    },
                    stockid: {
                        required: "Please Enter Stock ID"
                    },
                    grand_total: {
                        required: "Add Stock Items"
                    },
                    bill_no: {
                        required: "Please Enter Bill Number",
                        minlength: "Bill Number must consist of at least 3 characters"
                    }
                }
            });

        });
        $(function () {
            $("#supplier").autocomplete("customer1.php", {
                width: 160,
                autoFill: true,
                selectFirst: true
            });
            
            $("#supplier").blur(function () {


                $.post('check_customer_details.php', {stock_name1: $(this).val()},
                    function (data) {

                        $("#address").val(data.address);
                        $("#contact1").val(data.contact1);

                        if (data.address != undefined)
                            $("#0").focus();

                    }, 'json');


            });
           $('#test1').jdPicker({
			});


            var hauteur = 0;
            $('.code').each(function () {
                if ($(this).height() > hauteur) hauteur = $(this).height();
            });

            $('.code').each(function () {
                $(this).height(hauteur);
            });
        });

        function numbersonly(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
            if (unicode != 8 && unicode != 46 && unicode != 37 && unicode != 38 && unicode != 39 && unicode != 40) { //if the key isn't the backspace key (which we should allow)
                if (unicode < 48 || unicode > 57)
                    return false
            }
        }
        function edit_stock_details(id) {
            document.getElementById('display').style.display = "block";

            document.getElementById('particular').value = document.getElementById(id + 'pn').value;
            document.getElementById('item').value = document.getElementById(id + 'st').value;
            document.getElementById('job_no').value = document.getElementById(id + 'jn').value;
			document.getElementById('a').value = document.getElementById(id + 'q1').value;
            document.getElementById('b').value = document.getElementById(id + 'q2').value;
			document.getElementById('c').value = document.getElementById(id + 'q3').value;
            // if(document.getElementById(id + 'q3').checked){
            //     $('#c').attr('checked',true);
            //     document.getElementById('c').value = "yes";
            //     }else{
            //         $('#c').removeAttr('checked');
            //         document.getElementById('c').value = "no";
            //     }
            document.getElementById('rates').value = document.getElementById(id + 'rt').value;
            document.getElementById('quty').value = document.getElementById(id + 'q').value;
            document.getElementById('sell').value = document.getElementById(id + 's').value;
            
            document.getElementById('total').value = document.getElementById(id + 'to').value;
            document.getElementById('posnic_total').value = document.getElementById(id + 'to').value;

            document.getElementById('guid').value = id;
            document.getElementById('edit_guid').value = id;

        }
        function clear_data() {
            document.getElementById('display').style.display = "none";

            document.getElementById('item').value = "";
			document.getElementById('a').value = "";
            document.getElementById('b').value = "";
			
            document.getElementById('c').value = "";
            document.getElementById('quty').value = "";
            document.getElementById('sell').value = "";
            document.getElementById('total').value = "";
            document.getElementById('posnic_total').value = "";

            document.getElementById('guid').value = "";
            document.getElementById('edit_guid').value = "";

        }
        function add_values() {
            if (unique_check()) {

                if (document.getElementById('edit_guid').value == "") {
                    if (document.getElementById('item').value != "" && document.getElementById('total').value != "") {
                        code = document.getElementById('item').value;
						a = document.getElementById('a').value;
						b = document.getElementById('b').value;
						c = document.getElementById('c').value;
                        quty = document.getElementById('quty').value;
                        sell = document.getElementById('sell').value;
                        total = document.getElementById('total').value;
                        item = document.getElementById('guid').value;
                        main_total = document.getElementById('posnic_total').value;

                         $('<tr id=' + item + '><td><input type=hidden value=' + item + ' id=' + item + 'id ><input type=text name="stock_name[]"  id=' + item + 'st style="width: 150px" class="round  my_with" ></td><td><input type=text name=quty[] readonly="readonly" value=' + quty + ' id=' + item + 'q class="round  my_with" style="text-align:right;" ></td><td><input type=text name=sell[] readonly="readonly" value=' + sell + ' id=' + item + 's class="round  my_with" style="text-align:right;"  ></td><td><input type=text name=stock[] readonly="readonly" value=' + disc + ' id=' + item + 'p class="round  my_with" style="text-align:right;" ></td><td><input type=text name=jibi[] readonly="readonly" value=' + total + ' id=' + item + 'to class="round  my_with" style="width: 120px;margin-left:20px;text-align:right;" ><input type=hidden name=total[] id=' + item + 'my_tot value=' + main_total + '> </td><td><input type=button value="" id=' + item + ' style="width:30px;border:none;height:30px;background:url(images/edit_new.png)" class="round" onclick="edit_stock_details(this.id)"  ></td><td><input type=button value="" id=' + item + ' style="width:30px;border:none;height:30px;background:url(images/close_new.png)" class="round" onclick= $(this).closest("tr").remove() ></td></tr>').fadeIn("slow").appendTo('#item_copy_final');
						document.getElementById('a').value = "";
						document.getElementById('b').value = "";
						document.getElementById('c').value = "";
						document.getElementById('quty').value = "";
                        document.getElementById('sell').value = "";
                        document.getElementById('total').value = "";
                        document.getElementById('item').value = "";
                        document.getElementById('guid').value = "";
                        if (document.getElementById('grand_total').value == "") {
                            document.getElementById('grand_total').value = main_total;
                        } else {
                            document.getElementById('grand_total').value = parseFloat(document.getElementById('grand_total').value) + parseFloat(main_total);
                        }
                        document.getElementById('main_grand_total').value = '$ ' + parseFloat(document.getElementById('grand_total').value).toFixed(2);
                        document.getElementById(item + 'st').value = code;
                        document.getElementById(item + 'to').value = total;

                    } else {
                        alert('Please Select An Item');
                    }
                } else {
                    id = document.getElementById('edit_guid').value;
                    document.getElementById(id + 'pn').value = document.getElementById('particular').value;
                    document.getElementById(id + 'st').value = document.getElementById('item').value;
                    document.getElementById(id + 'jn').value = document.getElementById('job_no').value;
					document.getElementById(id + 'q1').value = document.getElementById('a').value;
                    document.getElementById(id + 'q2').value = document.getElementById('b').value;
					
                    document.getElementById(id + 'rt').value = document.getElementById('rates').value;
                    document.getElementById(id + 'q3').value = document.getElementById('c').value;
                    // if(document.getElementById('c').checked){
                    //     $('#'+id+'q3').attr('checked',true);
                    //     document.getElementById(id + 'q3').value = "yes";
                    //     }else{
                    //         $('#'+id+'q3').removeAttr('checked');
                    //         document.getElementById(id + 'q3').value = "no";
                    //     }
                    document.getElementById(id + 'q').value = document.getElementById('quty').value;
                    document.getElementById(id + 's').value = document.getElementById('sell').value;

                    data1 = parseFloat(document.getElementById('grand_total').value) + parseFloat(document.getElementById('posnic_total').value) - parseFloat(document.getElementById(id + 'to').value);
                    document.getElementById('main_grand_total').value = data1;
                    document.getElementById('grand_total').value = data1;
					document.getElementById('payable_amount').value = data1;
                    document.getElementById(id + 'to').value = document.getElementById('total').value;
                    console.log();
// document.getElementById('grand_total').value=parseFloat(document.getElementById('grand_total').value)+parseFloat(document.getElementById('total').value);
//alert(data1);
//alert(parseFloat(document.getElementById(id+'my_tot').value));
//alert(parseFloat(document.getElementById('posnic_total').value));
                    balance_amount();

                    document.getElementById(id + 'my_tot').value = document.getElementById('posnic_total').value
					document.getElementById('a').value = "";
                    document.getElementById('b').value = "";
                    document.getElementById('c').value = "";
                    document.getElementById('quty').value = "";
                    document.getElementById('rates').value - "";
                    document.getElementById('sell').value = "";
                    document.getElementById('total').value = "";
                    document.getElementById('item').value = "";
                    document.getElementById('guid').value = "";
                    document.getElementById('edit_guid').value = "";
                }
                document.getElementById('display').style.display = "none";

            }
            discount_amount();
        }
        function unique_check() {
            if (!document.getElementById(document.getElementById('guid').value) || document.getElementById('edit_guid').value == document.getElementById('guid').value) {
                return true;

            } else {

                alert("This Item is already added In This Purchase");
                document.getElementById('item').focus();
                id = document.getElementById('edit_guid').value;

                document.getElementById('item').focus();
                document.getElementById('item').value = document.getElementById(id + 'st').value;
                document.getElementById('quty').value = document.getElementById(id + 'q').value;
                document.getElementById('sell').value = document.getElementById(id + 's').value;
                document.getElementById('total').value = document.getElementById(id + 'to').value;
                document.getElementById('guid').value = id;
                document.getElementById('edit_guid').value = id;
                return false;


            }
        }
        function total_amount() {


            document.getElementById('total').value = document.getElementById('sell').value
            document.getElementById('posnic_total').value = document.getElementById('total').value;
            // document.getElementById('total').value = '$ ' + parseFloat(document.getElementById('total').value).toFixed(2);
            balance_amount();
        }
        function balance_amount() {
            if (document.getElementById('grand_total').value != "" && document.getElementById('payment').value != "") {
                data = parseFloat(document.getElementById('grand_total').value);
                document.getElementById('balance').value = data - parseFloat(document.getElementById('payment').value);
                console.log();
                if (parseFloat(document.getElementById('grand_total').value) >= parseFloat(document.getElementById('payment').value)) {

                    document.getElementById('balance').value = parseFloat(document.getElementById('grand_total').value) - parseFloat(document.getElementById('payment').value);
                } else {
                    if (document.getElementById('grand_total').value != "") {
                        document.getElementById('balance').value = '000.00';
                        document.getElementById('payment').value = parseFloat(document.getElementById('grand_total').value);
                    } else {
                        document.getElementById('balance').value = '000.00';
                        document.getElementById('payment').value = "";
                    }
                }
            } else {
                document.getElementById('balance').value = "";
            }


        }
        function quantity_chnage(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
            if (unicode != 13 && unicode != 9) {
            }
            else {
                add_values();

            }
            if (unicode != 27) {
            }
            else {

                document.getElementById("item").focus();
            }
        }

        function numbersonly(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
            if (unicode != 8 && unicode != 46 && unicode != 37 && unicode != 27 && unicode != 38 && unicode != 39 && unicode != 40 && unicode != 9) { //if the key isn't the backspace key (which we should allow)
                if (unicode < 48 || unicode > 57)
                    return false
            }
        }
       
        function discount_amount() {

           
            document.getElementById('payable_amount').value = parseFloat(document.getElementById('grand_total').value);
            if (parseFloat(document.getElementById('payment').value) > parseFloat(document.getElementById('payable_amount').value)) {
                document.getElementById('payment').value = parseFloat(document.getElementById('payable_amount').value);

            }

        }
        function reduce_balance(id) {
            var minus = parseFloat(document.getElementById(id + "my_tot").value);
            document.getElementById('grand_total').value = parseFloat(document.getElementById('grand_total').value) - minus;
            document.getElementById('main_grand_total').value = '$ ' + parseFloat(document.getElementById('grand_total').value).toFixed(2);
            discount_amount();
            //console.log(id);
        }
        
    </script>

</head>
<body>

<!-- TOP BAR -->
<?php include_once("tpl/top_bar.php"); ?>
<!-- end top-bar -->


<!-- HEADER -->
<?php include_once("tpl/header.php"); ?>
<!-- end header -->


<!-- MAIN CONTENT -->
<div id="content">

    <div class="page-full-width cf">

        <div class="side-menu fl">

            <h3>Sales Management</h3>
            <ul>
                <li><a href="add_sales.php">Add Sales</a></li>
                <li><a href="view_sales.php">View Sales</a></li>

            </ul>

        </div>
        <!-- end side-menu -->

        <div class="side-content fr">

            <div class="content-module">

                <div class="content-module-heading cf">

                    <h3 class="fl">Update sales</h3>
                    <span class="fr expand-collapse-text">Click to collapse</span>
                    <span class="fr expand-collapse-text initial-expand">Click to expand</span>

                </div>
                <!-- end content-module-heading -->

                <div class="content-module-main cf">

                    <?php
                    if (isset($_POST['supplier']) and isset($_POST['stock_name'])) {
                        $billnumber = mysqli_real_escape_string($db->connection, $_POST['bill_no']);
                        $autoid1 = mysqli_real_escape_string($db->connection, $_POST['id']);

                        $customer = mysqli_real_escape_string($db->connection, $_POST['supplier']);
                        $address = mysqli_real_escape_string($db->connection, $_POST['address']);
                        $contact = mysqli_real_escape_string($db->connection, $_POST['contact']);
                        $count = $db->countOf("customer_details", "customer_name='$customer'");
                        if ($count == 0) {
                            $db->query("insert into customer_details(customer_name,customer_address,customer_contact1) values('$customer','$address','$contact')");
                        }
                        $payment = mysqli_real_escape_string($db->connection, $_POST['payment']);
                        $balance = mysqli_real_escape_string($db->connection, $_POST['balance']);

                      $newvalue = $balance;
                       $oldvalue = $db->queryUniqueValue("SELECT balance FROM customer_details WHERE customer_name='$customer'");
					   $oldbalance = $db->queryUniqueValue("SELECT balance FROM stock_sales WHERE id='$autoid1'");
                        
						if($newvalue >= $oldbalance){
							$diff = (int)$newvalue - (int)$oldbalance;
							
						}
						else{
							$diff = (int)$oldbalance - (int)$newvalue;
                      	
						}
						$temp_balance = (int)$oldvalue + (int)$diff;
                        $db->execute("UPDATE customer_details SET balance='$temp_balance' WHERE customer_name='$customer'");
                        
                        $mode = mysqli_real_escape_string($db->connection, $_POST['mode']);


                        $namet = $_POST['stock_name'];
                       
                        $ratet = $_POST['sell'];
                        

                        $totalt = $_POST['total'];
                        $payable = mysqli_real_escape_string($db->connection, $_POST['subtotal']);
                        
                        $subtotal = mysqli_real_escape_string($db->connection, $_POST['payable']);

                        $username = $_SESSION['username'];

                        $i = 0;
                        $j = 1;

                        for ($i = 0; $i < count($namet); $i++){
							$autoid = $_POST['s_id'][$i];
                            $name1 = $namet[$i];
                            $particular = $_POST['particular_name'][$i];
                            $rates = $_POST['rates'][$i];
                            $job_no = $_POST['job_no'][$i];
							$siz = $_POST['a'][$i];
							$papr = $_POST['b'][$i];
							$remak = $_POST['c'][$i];
                            $quantity = $_POST['quantity'][$i];
                            $rate = $_POST['sell'][$i];
                            $total = $_POST['total'][$i];
                            $selected_date = $_POST['date'];
                            $selected_date = strtotime($selected_date);
                            $mysqldate = date('Y-m-d H:i:s', $selected_date);
                            $username = $_SESSION['username'];

                           

                                $old_quantity = $db->queryUniqueValue("SELECT quantity FROM stock_sales WHERE id='$autoid' and count1='$i'");
                                $db->query("update stock_sales set rates='$rates',particular='$particular',job_no='$job_no',pulty='$remak',size='$siz',paper='$papr',remark='-',grand_total='$payable', stock_name='$name1',selling_price='$rate',quantity='$quantity',amount='$total',date='$mysqldate',username='$username',customer_id='$customer',subtotal='$subtotal',payment='$payment',balance='$balance',mode='$mode',description='$description',billnumber='$billnumber' where id='$autoid'");
                              
                        }
                        $trans_id = trim($_POST['stockid']);
                        echo "<div style='background-color:yellow;'><br><font color=green size=+1 >Sales Updated ! Transaction ID [ $autoid ]</font></div> ";
                      
                    }
                  

                    ?>
                    <?php
                    if (isset($_GET['sid']))
                        $id = $_GET['sid'];

                    $line = $db->queryUniqueObject("SELECT * FROM stock_sales WHERE id='$id'");
                    ?>
                    <form name="form1" method="post" id="form1" action="">
                        <input type="hidden" id="posnic_total">
                        <input type="hidden" name="id" value="<?php echo $id ?>">

                        <table class="form" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <?php
                                $max = $db->maxOfAll("id", "stock_sales");
                                $max = $max + 1;
                                $autoid = "SD" . $max . "";
                                ?>
                                <td>Stock ID:</td>
                                <td><input name="stockid" type="text" id="stockid" readonly maxlength="200"
                                           class="round default-width-input" style="width:130px "
                                           value="<?php echo $line->jobid; ?>"/></td>

                                <td>Date:</td>
                                <td><input name="date" id="test1" placeholder="" value="<?php echo $line->date; ?> "
                                           type="text" id="name" maxlength="200" class="round default-width-input"/>
                                </td>
                                <td><span class="man">*</span>Bill No:</td>
                                <td><input name="bill_no" placeholder="ENTER BILL NO" type="text" id="bill_no"
                                           maxlength="200" value="<?php echo $line->billnumber; ?> "
                                           class="round default-width-input" style="width:120px "/></td>

                            </tr>
                            <tr>
                                <td><span class="man">*</span>Supplier:</td>
                                <td><input name="supplier" placeholder="ENTER SUPPLIER" type="text" id="supplier"
                                           value="<?php echo $line->customer_id; ?> " maxlength="200"
                                           class="round default-width-input" style="width:130px "/></td>

                                <td>Address:</td>
                                <td><input name="address" placeholder="ENTER ADDRESS" type="text"
                                           value="<?php $quantity = $db->queryUniqueValue("SELECT customer_address FROM customer_details WHERE customer_name='" . $line->customer_id . "'");
                                           echo $quantity; ?>" id="address" maxlength="200"
                                           class="round default-width-input"/></td>

                                <td>contact:</td>
                                <td><input name="contact" placeholder="ENTER CONTACT" type="text"
                                           value="<?php $quantity = $db->queryUniqueValue("SELECT customer_contact1 FROM customer_details WHERE customer_name='" . $line->customer_id . "'");
                                           echo $quantity; ?>" id="contact1" maxlength="200"
                                           class="round default-width-input" onkeypress="return numbersonly(event)"
                                           style="width:120px "/></td>

                            </tr>
                        </table>
                        <input type="hidden" id="guid">
                        <input type="hidden" id="edit_guid">

                        <table id="hideen_display">
                            <tr>
                                <tr>
                                
                                <th style="width: 150px">Product:</th>
                                <th>Job No:</th>
								<th>Size:</th>
                                <th>Paper:</th>
                                <th>Pulty:</th>
                                <th>Other:</th>
                                <th>Quantity:</th>
                                <th>Rates:</th>
                                <th>Price:</th>
                                <th>&nbsp; </th>
                                <th>&nbsp; </th>
                                <th>&nbsp; </th>
								<th>&nbsp;</td>
                            </tr>
                        </table>
                        <table class="form" id="display" style="display:none">
                            <tr>
                            
                                <td ><input name="" type="text" id="item" maxlength="200" style="width: 150px" class="round my_with "
                                           
                                           value="<?php echo isset($supplier) ? $supplier : ''; ?>"/></td>
                                <td ><input name="" type="text" id="job_no" maxlength="200" class="round my_with "
                                
                                value=" " style="width: 80px"/></td>
								<td><input name="" type="text" id="a" maxlength="200"
                                           class="round default-width-input " style="width: 80px" value="<?php echo isset($category) ? $category : ''; ?>"/></td>		   
								<td><input name="" type="text" id="b" maxlength="200"
                                           class="round default-width-input " style="width: 80px" value="<?php echo isset($category) ? $category : ''; ?>"/></td>
                                <td><input name="" type="text" id="c" maxlength="200"
                                           class="round default-width-input " style="width: 80px" value="<?php echo isset($category) ? $category : ''; ?>"/></td>
                                <td ><input name="" type="text" id="particular" maxlength="200" class="round my_with "
                                
                                value=" " style="width: 80px"/></td>
                                <td><input name="" type="text" id="quty" maxlength="200" class="round  my_with"
                                           onkeyup="total_amount();"
                                           value="<?php echo isset($category) ? $category : ''; ?>"/></td>

                                <td ><input name="" type="text" id="rates" maxlength="200" class="round my_with " value=" " style="width: 80px"/></td>
                                <td><input name="" type="text" id="sell"  maxlength="200"
                                           class="round  my_with"
                                           value="<?php echo isset($category) ? $category : ''; ?>" onkeyup="total_amount()";/></td>
								<td><input name="" type="hidden" id="total" readonly maxlength="200"
                                           class="round default-width-input " style="width:80px;  margin-left: 20px"
                                           value="<?php echo isset($category) ? $category : ''; ?>" /></td>
                                <td><input type="button" onclick="add_values()" onkeyup=" balance_amount();"
                                           id="add_new_code"
                                           style="margin-left:20px; width:30px;height:30px;border:none;background:url(images/save.png)"
                                           class="round"></td>
                                <td><input type="button" value="" id="cancel" onclick="clear_data()"
                                           style="width:30px;float: right; border:none;height:30px;background:url(images/close_new.png)">
                                </td>
                            </tr>
                        </table>
                        <input type="hidden" id="guid">
                        <input type="hidden" id="edit_guid">


                        <div style="overflow:auto ;max-height:300px;  ">
                            <table class="form" id="item_copy_final">

                                <?php
                                $sid = $line->transactionid;
                              //  $max = $db->maxOf("count1", "stock_sales", "transactionid ='$sid'");
								 
								 
                                //for ($i = 1; $i <= $max; $i++) {
                                   $line1 = "SELECT * FROM stock_sales WHERE transactionid ='$sid' ";
									$result = mysqli_query($db->connection, $line1);
                                    $item = $db->queryUniqueValue("SELECT transactionid  FROM stock_sales WHERE stock_name='" . $line->stock_name . "'");
                                 //  
								 while ($row = mysqli_fetch_array($result)) {
                                    ?>
                                    <tr>
                                       
                                        <td><input name="stock_name[]" type="text" id="<?php echo $row['id']; ?>st"
                                                   maxlength="200" style="width: 150px" readonly
                                                   class="round "
                                                   value="<?php echo $row['stock_name']; ?>"/></td>
                                        <td><input name="job_no[]" type="text" id="<?php echo $row['id']; ?>jn"
                                                   maxlength="200" style="width: 80px" readonly
                                                   class="round "
                                                   value="<?php echo $row['job_no']; ?>"/></td>
                                         <td><input name="a[]" type="text" id="<?php echo $row['id']; ?>q1"
                                                   maxlength="200" class="round my_with"
                                                   value="<?php echo $row['size']; ?>" readonly
                                                   onkeypress="return numbersonly(event)"/></td>
										 <td><input name="b[]" type="text" id="<?php echo $row['id']; ?>q2"
                                                   maxlength="200" class="round my_with"
                                                   value="<?php echo $row['paper']; ?>" readonly
                                                   onkeypress="return numbersonly(event)"/></td>
                                         <td><input name="c[]" type="text" id="<?php echo $row['id']; ?>q3"
                                                   maxlength="200" class="round my_with"
                                                   value="<?php echo $row['pulty']; ?>" readonly
                                                   /></td>
                                        <td><input name="particular_name[]" type="text" id="<?php echo $row['id']; ?>pn"
                                        maxlength="200" style="width: 80px" readonly
                                        class="round "
                                        value="<?php echo $row['particular']; ?>"/></td>

                                        <td><input name="quantity[]" type="text" id="<?php echo $row['id']; ?>q"
                                                   maxlength="200" class="round my_with"
                                                   value="<?php echo $row['quantity']; ?>" readonly
                                                   onkeypress="return numbersonly(event)"/></td>

                                        <td><input name="rates[]" type="text" id="<?php echo $row['id']; ?>rt"
                                                maxlength="200" style="width: 80px" readonly
                                                class="round "
                                                value="<?php echo $row['rates']; ?>"/></td>
                                        <td><input type="hidden" name="s_id[]" value="<?php echo $row['id']; ?>">
                                        
                                        <input name="sell[]" type="text" id="<?php echo $row['id']; ?>s" maxlength="20" readonly class="round my_with" value="<?php echo $row['selling_price']; ?>" onkeypress="return numbersonly(event)"/></td>
                                        
                                        
                                         <td><input name="total[]" type="hidden" id="<?php echo $row['id']; ?>to" readonly maxlength="20" style="margin-left:20px;width: 120px" class="round "
                                                   value="<?php echo $row['amount']; ?>"/></td>
                                        <td><input type="hidden" id="<?php echo $row['id']; ?>" maxlength="20"
                                                   style="margin-left:20px;width: 120px" class="round "
                                                   value="<?php echo $row['amount']; ?>"/></td>
                                        <td><input type="hidden" id="<?php echo $row['id']; ?>"><input type="hidden" name="guid[]" value="<?php echo $row['id']; ?>">
                                        </td>
                                        <td><input type=button value="" id="<?php echo $row['id']; ?>"
                                                   style="width:30px;border:none;height:30px;background:url(images/edit_new.png)"
                                                   class="round" onclick="edit_stock_details(this.id)"></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>


                        <table class="form">

                            <tr>
                                
                                <td>&nbsp; </td>
                                <td>&nbsp; </td>
                                <td>Grand Total:<input type="hidden" readonly
                                                       value="<?php echo $line->grand_total; ?>" id="grand_total"
                                                       name="subtotal">
                                    <input type="text" id="main_grand_total" readonly
                                           value="<?php echo $line->grand_total; ?>" class="round default-width-input"
                                           style="text-align:right;width: 120px">
                                </td>
                                
                            </tr>
                            <tr>
                                <td>&nbsp; </td>
                                <td><input type="hidden" class="round" value="<?php echo $line->payment; ?>"
                                                   onkeyup=" balance_amount(); return numbersonly(event);"
                                                   name="payment" id="payment">
                                </td>
                                <td><input type="hidden" class="round" value="<?php echo $line->balance; ?>"
                                                   id="balance" name="balance">
                                </td>
                                <td>&nbsp; </td>
                                <td>&nbsp; </td>
                                <td>Payable Amount:<input type="hidden" readonly id="grand_total">
                                    <input type="text" id="payable_amount" value="<?php echo $line->subtotal; ?>"
                                           readonly="readonly" name="payable" class="round default-width-input"
                                           style="text-align:right;width: 120px">
                                </td>
                            </tr>
                        </table>
                       <table>
						<tr>
								<td>Current Status</td>
								<td><?php if($line->mode == "0"){ 								echo "In Process";								}
								else if($line->mode == "1"){									echo "Completed";									} 								else if($line->mode == "2"){									echo "Delivered";									} 																			?> </td>
						</tr>
                            <tr>
                                <td>Status &nbsp;</td>
                                <td>
                                    <select name="mode">
                                        <option value="0">In Process</option>
                                        <option value="1">Completed</option>																				<option value="2">Delivered</option>
                                        </select>
                                </td>
                                
                            </tr>
                        </table>
                        <table class="form">
                            <tr>
                                <td>
                                    <input class="button round blue image-right ic-add text-upper" type="submit"
                                           name="Submit" value="Add">
                                </td>
                                <td> (Control + S)
                                    <input class="button round red   text-upper" type="reset" name="Reset"
                                           value="Reset"></td>
                                <td>&nbsp; </td>
                                <td>&nbsp; </td>
                            </tr>
                        </table>
                    </form>


                </div>
                <!-- end content-module-main -->


            </div>
            <!-- end content-module -->


        </div>
        <!-- end full-width -->

    </div>
    <!-- end content -->


<!-- FOOTER --><div id="footer">
    <p>Any Queries email to <a href="mailto:jeetsarphare@outlook.com?subject=Print_gallery">jeetsarphare@outlook.com</a>.
    </p>

</div>
<!-- end footer -->
</body>
</html>
<?php
include_once("init.php"); // Use session variable on this page. This function must put on the top of page.
if (!isset($_SESSION['username']) || $_SESSION['usertype'] != 'admin') { // if session variable "username" does not exist.
    header("location: index.php?msg=Please%20login%20to%20access%20admin%20area%20!"); // Re-direct to index.php
} else {
    if (isset($_GET['from_sales_date1']) && isset($_GET['to_sales_date1']) && $_GET['from_sales_date1'] != '' && $_GET['to_sales_date1'] != '') {

        error_reporting(0);
        $cust1 = $_GET['selectcust'];
        //$cust1 = html_entity_decode($cust1, ENT_QUOTES);
        /* $selected_date = $_GET['from_sales_date1'];
        $selected_date = strtotime($selected_date);
        $mysqldate = date('Y-m-d', $selected_date);
        $fromdate = $selected_date;
		
        $selected_date1 = $_GET['to_sales_date1'];
        $selected_date1 = strtotime($selected_date1);
        $mysqldate1 = date('Y-m-d', $selected_date1);
        $todate = $mysqldate1;
 */
        $cust1 = $db->queryUniqueValue("SELECT customer_name FROM customer_details where id=$cust1");
        $selected_date = $_GET['from_sales_date1'];

        $selected_date = strtotime($selected_date);

        $mysqldate = date('Y-m-d H:i:s', $selected_date);

        $fromdate = $mysqldate;

        $selected_date = $_GET['to_sales_date1'];

        $selected_date = strtotime($selected_date);

        $mysqldate = date('Y-m-d H:i:s', $selected_date);



        $todate = $mysqldate;


?>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html>

        <head>
            <title>Sale Report</title>
            <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
            <script src="js/script.js"></script>
            <style type="text/css" media="print">
                .hide {
                    display: none
                }
            </style>
            <script type="text/javascript">
                function printpage(printarea) {

                    var printContents = document.getElementById(printarea).innerHTML;
                    var originalContents = document.body.innerHTML;

                    document.body.innerHTML = printContents;

                    window.print();

                    document.body.innerHTML = originalContents;
                }
            </script>
            <style type="text/css">
                body {
                font-family: Arial;
                font-size: 14px;
                }
                .report-tbl {
                font-size: 12px;
                }
                .report-tbl td{
                padding: 5px;
                }
            </style>
        </head>

        <body>


            <input name="print" type="button" value="Print" id="printButton" onClick="printpage('printarea')">
            <div id="printarea">
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center">
                            <div align="left" style="display: none;">
                                <?php $line4 = $db->queryUniqueObject("SELECT * FROM store_details ");
                                ?>
                                <strong><?php echo $line4->name; ?></strong><br />
                                <?php echo $line4->address; ?>,<?php echo $line4->place; ?>, <br />
                                <?php echo $line4->city; ?>,<?php echo $line4->pin; ?><br />
                                Website<strong>:<?php echo $line4->web; ?></strong><br>Email<strong>:<?php echo $line4->email; ?></strong><br />Phone
                                <strong>:<?php echo $line4->phone; ?></strong>
                                <br />
                                <?php ?>
                            </div>
                        </td>
                    </tr>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <tr>
                            <td height="30" align="center"><strong>Sales Report </strong></td>
                        </tr>
                        <tr>
                            <td height="30" align="center"><?php echo $cust1; ?> </td>
                        </tr>
                        <tr>
                            <td height="30" align="center">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="right">
                                <table width="300" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="150"><strong>Total Sales </strong></td>
                                        <td width="150">

                                            <?php
                                           
                                            $line1 = "SELECT DISTINCT transactionid FROM stock_sales WHERE customer_id='$cust1'  ORDER BY date ASC";
                                            $result = mysqli_query($db->connection, $line1);

                                            while ($row = mysqli_fetch_array($result)) {

                                                $item = $db->queryUniqueValue("SELECT subtotal FROM stock_sales WHERE transactionid='" . $row['transactionid'] . "' AND date BETWEEN '$fromdate' AND '$todate' ");
                                                $a = $a + $item;
                                            }


                                            ?>
                                            &nbsp;<?php
                                                    echo  "Rs." . $a; ?></td>
                                    </tr> <?php $paym = $db->queryUniqueValue("SELECT sum(payment) FROM transactions where customer='$cust1'  AND due BETWEEN  '$fromdate' AND '$todate' ");
                                            if ($paym == "") {
                                                $paym = 0;
                                            }                                            ?>
                                    <tr>
                                        <td><strong>Received Amount </strong></td>

                                        <td width="150"><?php echo  "Rs." . $paym; ?></td>
                                    </tr>
                                    <tr> <?php $bal = $a - $paym; ?>
                                        <td width="150"><strong>Total OutStanding </strong></td>
                                        <td width="150"><?php echo  "Rs." . $bal; ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td width="45">
                                <hr>
                            </td>
                        </tr>
                        <tr>
                            <td height="20">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr> <?php $abc = $_GET['from_sales_date1'];
                                            $abc = strtotime($abc);
                                            $mdate = date("d/m/Y", $abc);
                                            $fdate = $mdate;
                                            $def = $_GET['to_sales_date1'];
                                            $def = strtotime($def);
                                            $mydate = date("d/m/Y", $def);

                                            $tdate = $mydate;
                                            ?>
                                        <td width="45"><strong>From</strong></td>
                                        <td width="393">&nbsp;<?php echo $fdate; ?></td>
                                        <td width="41"><strong>To</strong></td>
                                        <td width="116">&nbsp;<?php echo $tdate; ?></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td width="45">
                                <hr>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" border="1" cellspacing="0" cellpadding="0" class="report-tbl">
                                    <tr>
                                        <th><strong>Date</strong></th>
                                        <th><strong>Job No</strong></th>
                                        <th><strong>Item </strong></th>
                                        <th><strong>Quantity</strong></th>
                                        <th><strong>Size</strong></th>
                                        <th><strong>Paper</strong></th>
                                        <th><strong>Pulty</strong></th>
                                        <th><strong>Rate</strong></th>
                                        <th><strong>Other</strong></th>
                                        <th><strong>Amount</strong></th>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <?php
                                    $result7 = mysqli_query($db->connection, $line1);
                                    while ($row7 = mysqli_fetch_array($result7)) {

                                        $item7 = $db->query("SELECT * FROM stock_sales WHERE transactionid='" . $row7['transactionid'] . "' AND date BETWEEN '$fromdate' AND '$todate'");

                                        while ($line = $db->fetchNextObject($item7)) {
                                    ?>

                                            <tr>
                                                <td><?php $mysqldate2 = $line->date;
                                                    $phpdate = strtotime($mysqldate2);
                                                    $phpdate = date("d/m/Y", $phpdate);
                                                    echo $phpdate; ?></td>
                                                <td><?php echo $line->job_no; ?></td>
                                                <td><?php echo $line->stock_name; ?></td>
                                                <td><?php echo $line->quantity; ?></td>
                                                <td><?php echo $line->size; ?></td>
                                                <td><?php echo $line->paper; ?></td>
                                                <td><?php echo $line->pulty; ?></td>
                                                <td><?php echo $line->rates; ?></td>
                                                <td><?php echo $line->particular; ?></td>
                                                <td><?php echo $line->amount; ?></td>
                                            </tr>


                                    <?php
                                        }
                                    }
                                    ?><tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td><strong>Total :</strong> </td>
                                        <td><?php echo "Rs." . $a; ?></td>
                                    </tr>
                                </table>
                                <table width="595">
                                    <tr>
                                        <td height="30" align="center"><strong>Payment Report </strong></td>
                                    </tr>
                                </table>
                                <table width="100%" border="1" cellspacing="0" cellpadding="0" class="report-tbl">
                                    <tr>
                                        <td><strong>Date</strong></td>
                                        <td><strong>Mode </strong></td>
                                        <td><strong>Payment</strong></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr> <?php $line2 = "SELECT due,mode,payment FROM transactions WHERE customer='$cust1' AND type='sales' AND due BETWEEN '$fromdate' AND '$todate'  ORDER BY due ASC";
                                            $result9 = mysqli_query($db->connection, $line2);
                                            while ($row9 = mysqli_fetch_array($result9)) {  
                                            ?> 
                                            <tr>
                                            <td><?php $phpdate = strtotime($row9['due']);
                                                     $phpdate = date("d/m/Y", $phpdate);
                                                    echo $phpdate; ?></td>
                                            <!-- <td><?php //echo $row9['due']; ?></td> -->
                                            <td><?php echo $row9['mode']; ?></td>
                                            <td><?php echo $row9['payment']; ?></td>
                                    </tr> <?php                                    } 
                                                ?>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><strong>Total :</strong> </td>
                                        <td><?php echo "Rs." . $paym; ?></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>



                    </table>
                    </tr>
                </table>
            </div>
        </body>

        </html>
<?php
    } else
        echo "Please from and to date to process report";
}
?>
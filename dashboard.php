<?php
include_once("init.php");

?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Dashboard</title>

    <!-- Stylesheets -->

    <link rel="stylesheet" href="css/style.css">

    <!-- Optimize for mobile devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- jQuery & JS files -->
    <?php include_once("tpl/common_js.php"); ?>
    <script src="js/script.js"></script>
</head>
<body>

<!-- TOP BAR -->
<?php include_once("tpl/top_bar.php"); 		include_once("tpl/header.php"); ?>
<!-- end top-bar -->


<!-- HEADER -->



<!-- MAIN CONTENT -->
<div id="content">

    <div class="page-full-width cf">

        <div class="side-menu fl">

            <h3>Quick Links</h3>
            <ul>
                <li><a href="add_sales.php">Add Sales</a></li>
              <!--  <li><a href="">Add Purchase</a></li>
                <li><a href="add_supplier.php">Add Supplier</a></li> -->
                <li><a href="add_customer.php">Add Customer</a></li>
                <li><a href="view_report.php">Report</a></li>
            </ul>

        </div>
        <!-- end side-menu -->

        <div class="side-content fr">

            <div class="content-module">

                <div class="content-module-heading cf">

                    <h3 class="fl">Statistics</h3>
                    <span class="fr expand-collapse-text">Click to collapse</span>
                    <span class="fr expand-collapse-text initial-expand">Click to expand</span>

                </div>
                <!-- end content-module-heading -->

                <div class="content-module-main cf">
                <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $count = $db->countOfAll("stock_sales"); ?></h3>

              <p>Total No. Sales</p>
            </div>
            <div class="icon">
              <i class="fa fa-shopping-cart"></i>
            </div>
            <a href="#" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
       <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $count = $db->countOfAll("customer_details"); ?></h3>

              <p>Total Customers</p>
            </div>
            <div class="icon">
              <i class="fa fa-user"></i>
            </div>
            <a href="view_customers.php?page_name=customers" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
       
      </div>
      <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Price list</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Particulars</th>
                    <th>Price</th>
                    <th>Edit/Delete</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php 
								
								$sql = "SELECT * FROM  list_details ORDER BY id DESC";
								$result = mysqli_query($db->connection, $sql);
								$i = 1;
                                $no = $page - 1;
                                $no = $no * $limit;
                                while ($row = mysqli_fetch_array($result)) {
                                    ?>
                  <tr>
                    <td><?php echo $no + $i; ?></td>
                    <td><?php echo $row['list_name']; ?></td>
                    <td><?php echo $row['copy']; ?></td>
                    <td>
                    <a href="update_list_details.php?sid=<?php echo $row['id']; ?>&table=list_details&return=dashboard.php"
                                               class="table-actions-button ic-table-edit">
                                            </a>
                                            <a onclick="return confirmSubmit()"
                                               href="delete.php?id=<?php echo $row['id']; ?>&table=list_details&return=dashboard.php"
                                               class="table-actions-button ic-table-delete"></a>
                    </td>
                  </tr>
                  
                  <?php $i++;
                                } ?>
                  
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="add_list.php" class="btn btn-sm btn-info btn-flat pull-left">Add New</a>
              <!-- <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a> -->
            </div>
            <!-- /.box-footer -->
          </div>
			<!--	<marquee><p style="font-size:15px;color:blue">New Office Address : SHREE GANESH OFFSET, D-22 Ravi Estate, Rustam Mill Compund, Nr Gurudwara , Dudheshwar, Ahmedabad - 380 004 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Phone No: 079-25623771 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Mobile : +91-9374063771</p></marquee> -->

                                          <!-- <tr>
                            <td align="left">Total number of Suppliers</td>
                            <td align="left"><?php // echo $count = $db->countOfAll("supplier_details"); ?></td>
                        </tr> -->

                   
                    <!--<ul class="temporary-button-showcase">
                        <li><a href="#" class="button round blue image-right ic-add text-upper">Add</a></li>
                        <li><a href="#" class="button round blue image-right ic-edit text-upper">Edit</a></li>
                        <li><a href="#" class="button round blue image-right ic-delete text-upper">Delete</a></li>
                        <li><a href="#" class="button round blue image-right ic-download text-upper">Download</a></li>
                        <li><a href="#" class="button round blue image-right ic-upload text-upper">Upload</a></li>
                        <li><a href="#" class="button round blue image-right ic-favorite text-upper">Favorite</a></li>
                        <li><a href="#" class="button round blue image-right ic-print text-upper">Print</a></li>
                        <li><a href="#" class="button round blue image-right ic-refresh text-upper">Refresh</a></li>
                        <li><a href="#" class="button round blue image-right ic-search text-upper">Search</a></li>
                    </ul>-->

                </div>
                <!-- end content-module-main -->


            </div>
            <!-- end content-module -->


        </div>
        <!-- end full-width -->

    </div>
</div>


<!-- FOOTER -->

<!-- FOOTER --><div id="footer">
    <p>Any Queries email to <a href="mailto:jeetsarphare@outlook.com?subject=Print_gallery">jeetsarphare@outlook.com</a>.
    </p>

</div>
<!-- end footer -->
</body>
</html>
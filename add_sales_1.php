<html>
	 <head>
	 <meta charset="utf-8">
    <title>POSNIC - Add Stock Category</title>

    <!-- Stylesheets -->

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="js/date_pic/date_input.css">
    <link rel="stylesheet" href="lib/auto/css/jquery.autocomplete.css">

    <!-- Optimize for mobile devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- jQuery & JS files -->
    <?php include_once("tpl/common_js.php"); ?>
    <script src="js/script.js"></script>
    <script src="js/date_pic/jquery.date_input.js"></script>
    <script src="lib/auto/js/jquery.autocomplete.js "></script>

	 </head>
<body>

<!-- TOP BAR -->
<div id="content">

    <div class="page-full-width cf">

        <div class="side-menu fl">

            <h3>Sales Management</h3>
            <ul>
                <li><a href="add_sales.php">Add Sales</a></li>
                <li><a href="view_sales.php">View Sales</a></li>
            </ul>

        </div>
        <!-- end side-menu -->

        <div class="side-content fr">

            <div class="content-module">

                <div class="content-module-heading cf">

                    <h3 class="fl">Add Sales</h3>
                    <span class="fr expand-collapse-text">Click to collapse</span>
                    <span class="fr expand-collapse-text initial-expand">Click to expand</span>

                </div>
                <!-- end content-module-heading -->

                <div class="content-module-main cf">

 <?php
include_once("init.php");

?>
 <?php
                    //Gump is libarary for Validatoin
                    if (isset($_GET['msg'])) {
                        echo $_GET['msg'];
                    }

                    if (isset($_POST['payment'])) {
                        $_POST = $gump->sanitize($_POST);

                        $gump->validation_rules(array(
                            'payment' => 'required|max_len,100|min_len,1'


                        ));

                        $gump->filter_rules(array(
                            'payment' => 'trim|sanitize_string|mysqli_escape'


                        ));

                        $validated_data = $gump->run($_POST);
                        $stock_name = "";
                        $stockid = "";
                        $payment = "";
                        $bill_no = "";
						$size="";
						$paper="";


                        if ($validated_data === false) {
                            echo $gump->get_readable_errors(true);
                        } else {
                            $username = $_SESSION['username'];

                            $stockid = mysqli_real_escape_string($db->connection, $_POST['stockid']);
							$jobid = mysqli_real_escape_string($db->connection, $_POST['jobid']);
							

                            $bill_no = mysqli_real_escape_string($db->connection, $_POST['bill_no']);
                            $customer = mysqli_real_escape_string($db->connection, $_POST['supplier']);
                            $address = mysqli_real_escape_string($db->connection, $_POST['address']);
                            $contact = mysqli_real_escape_string($db->connection, $_POST['contact']);
							 $count = $db->countOf("customer_details", "customer_name='$customer'");
                            if ($count == 0) {
                                $db->query("insert into customer_details(customer_name,customer_address,customer_contact1) values('$customer','$address','$contact')");
                            }
                            $stock_name = $_POST['stock_name'];
                            $particular = $_POST['particular_name'];
                            $job_no = $_POST['job_no'];
							$size = $_POST['a'];
							$paper = $_POST['b'];
							$remark = $_POST['c'];
                            $quty = $_POST['quty'];
                            $ates = $_POST['rates'];
                            $date = mysqli_real_escape_string($db->connection, $_POST['date']);
                            $sell = $_POST['sell'];
                            $total = $_POST['total'];
                            $payable = $_POST['subtotal'];
                            $payment = mysqli_real_escape_string($db->connection, $_POST['payment']);
                            
                            $subtotal = mysqli_real_escape_string($db->connection, $_POST['payable']);
                            $balance = mysqli_real_escape_string($db->connection, $_POST['balance']);
                            $mode = mysqli_real_escape_string($db->connection, $_POST['mode']);
                            
                            $temp_balance = $db->queryUniqueValue("SELECT balance FROM customer_details WHERE customer_name='$customer'");
                            $temp_balance = (int)$temp_balance + (int)$balance;
                            $db->execute("UPDATE customer_details SET balance=$temp_balance WHERE customer_name='$customer'");
                            
                           // $max = $db->maxOfAll("id", "stock_sales");
                           // $max = $max + 1;
                            $autoid = "PM" . $jobid . "";
                            for ($i = 0; $i < count($stock_name); $i++) {
                                $name1 = $stock_name[$i];
                                $particular = $_POST['particular_name'][$i];
                                $job_no = $_POST['job_no'][$i];
								$siz = $_POST['a'][$i];
								$papr = $_POST['b'][$i];

                                $count_stock = $db->countOf("stock_details", "stock_name='$siz' AND stock_type='$papr' ");
                                if ($count_stock == 0) {
                                    $max = $db->maxOfAll("id", "stock_details");
                                    $max = $max + 1;
                                    $autoid = "SD" . $max . "";
                                    $db->query("insert into stock_details(stock_id,stock_name,stock_type,stock_quatity,category) values('$autoid','$siz','$papr','0','-')");
                                }
                                $remak = $_POST['c'][$i];
                                $rates = $_POST['rates'][$i];
                                $quantity = $_POST['quty'][$i];
                                $rate = $_POST['sell'][$i];
                                $total = $_POST['total'][$i];
								
								$selected_date = $_POST['date'];
                                $selected_date = strtotime($selected_date);
                                $mysqldate = date('Y-m-d H:i:s', $selected_date);
                                $username = $_SESSION['username'];

                            $db->query("insert into stock_sales (rates,particular,job_no,pulty,size,paper,remark,grand_total,transactionid,jobid,stock_name,selling_price,quantity,amount,date,username,customer_id,subtotal,payment,balance,mode,count1,billnumber)
                            values('$rates','$particular','$job_no','$remak','$siz','$papr','-','$payable','$stockid','$autoid','$name1','$rate','$quantity','$total','$mysqldate','$username','$customer','$subtotal','$payment','$balance','$mode',$i+1,'$bill_no')");

                                    
                            }
						//	$mes = "Hello ".$customer.",Your order is confirmed with Job id :".$autoid." and your payable amount is R.s ".$payable." FROM : SHREE GANESH OFFSET " ;
                           
							//header("Location:http://sms.zorens.com/api/sendhttp.php?authkey=3786AOS4M9vo6Pbz58d8b51e&mobiles=919033098795&message=Hellodemo&sender=ABCDEF&route=4&country=0");
                           
//Your authentication key
//$authKey = "3786AOS4M9vo6Pbz58d8b51e";

//Multiple mobiles numbers separated by comma
//$mobileNumber = "91".$contact;

//Sender ID,While using route4 sender id should be 6 characters long.
//$senderId = "SGOFST";

//Your message to send, Add URL encoding here.
//$message = urlencode("Test message");

//Define route 
//$route = "4";
//$country = "0";
//Prepare you post parameters
//$postData = array(
  //  'authkey' => $authKey,
 //   'mobiles' => $mobileNumber,
  //  'message' => $mes,
 //   'sender' => $senderId,
 //   'route' => $route,
//	'country' => $country
//);

//API URL
//$url="http://sms.zorens.com/api/sendhttp.php";

// init the resource
//$ch = curl_init();
//curl_setopt_array($ch, array(
  //  CURLOPT_URL => $url,
  //  CURLOPT_RETURNTRANSFER => true,
 //   CURLOPT_POST => true,
 //   CURLOPT_POSTFIELDS => $postData
  //  //,CURLOPT_FOLLOWLOCATION => true
//));


//Ignore SSL certificate verification
//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


//get response
//$output = curl_exec($ch);

//Print error if any
//if(curl_errno($ch))
//{
//    echo 'error:' . curl_error($ch);
//}

//curl_close($ch);

							
							$msg = "<br><font color=green size=6px >Sales Added successfully Ref: [" . $_POST['autoid'] . "] !</font>";
							echo "<script>window.location= 'add_sales.php?msg=$msg';</script>";
                            
                        }

                    }

                    ?>


                </div>
                <!-- end content-module-main -->


            </div>
            <!-- end content-module -->


        </div>
    </div>
    <!-- end full-width -->

</div>
<div id="footer">
    <p>Any Queries email to <a href="mailto:jeetsarphare@outlook.com?subject=Print_gallery">jeetsarphare@outlook.com</a>.
    </p>

</div>
<!-- end footer -->

</body>
</html>
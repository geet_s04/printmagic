<?php $html =
  '<html><body>'.
  '<p>Put your html here, or generate it with your favourite '.
  'templating system.</p>'.
  '</body></html>'; 
function pdf_create($html, $filename='', $stream=TRUE) 
{
    require_once('dompdf/dompdf_config.inc.php');
    $savein = '';
    $dompdf = new DOMPDF();
    $dompdf->load_html($html);
    $dompdf->render();
    $canvas = $dompdf->get_canvas();
    $font = Font_Metrics::get_font("arial", "normal","12px");

    /*// the same call as in my previous example
    $canvas->page_text(540, 773, "Page {PAGE_NUM} of {PAGE_COUNT}",
                   $font, 6, array(0,0,0));*/

    $pdf = $dompdf->stream("sample.pdf");

    unset($html);
    unset($dompdf); 

}

pdf_create($html);

?>